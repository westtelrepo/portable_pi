use structopt::StructOpt;

#[derive(StructOpt, Debug)]
pub struct Opt {
    #[structopt(subcommand)]
    pub cmd: OptCommand,
}

#[derive(StructOpt, Debug)]
pub enum OptCommand {
    /// DO NOT USE! Runs in initramfs
    InitramfsLocalBottom,
    /// only use in IWD service file, copies info from /raspifirm/data/iwd
    InitIwd,
    /// only use in systemd-networkd service file, copies info from /raspifirm/data/network
    InitNetworkd {
        /// are we in systemd-networkd's ExecStartPre? if so, don't restart
        #[structopt(long)]
        in_exec_pre: bool,
    },
    /// upgrade the system to a new version
    AbUpgrade {
        /// new version to upgrade to
        #[structopt(long)]
        new_version: String,
        /// url to find files
        #[structopt(long, default_value = "http://[fd97:5e16:a609:1::1]")]
        url: String,
        /// size of the new root partition, suitiable for `lvcreate -L`
        #[structopt(long, default_value = "500M")]
        new_root_size: String,
    },
    /// remove data for all non-current versions
    /// WARNING: has no prompts!
    #[structopt(name = "ab-gc")]
    AbGc,
    /// remount /raspifirm rw, run command, remount ro
    WithRaspifirmLock {
        /// command to run with /raspifirm rw
        #[structopt(default_value = "bash")]
        command: Vec<String>,
    },
    /// Get current AB version
    AbVersion,
    /// upgrade raspberry pi firmware
    FirmwareUpgrade {
        /// new version to upgrade to
        #[structopt(long)]
        new_version: String,
        /// url to find files
        #[structopt(long, default_value = "http://[fd97:5e16:a609:1::1]")]
        url: String,
    },
    /// touch /raspifirm/reset.txt, reset data partition next boot
    ///
    /// WARNING: deletes all data not in /raspifirm!
    ResetNextBoot,
    /// Test the resolver, resolves _wireguard._udp.<hostname>.
    ///
    /// Requires systemd-resolved.
    WgResolve { hostname: String },
}
