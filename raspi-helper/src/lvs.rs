use duct::cmd;
use serde::Deserialize;

#[derive(Deserialize)]
struct LvReport {
    report: Vec<LvEntry>,
}

#[derive(Deserialize)]
struct LvEntry {
    lv: Vec<ActualLvEntry>,
}

#[derive(Deserialize)]
struct ActualLvEntry {
    lv_name: String,
}

// TODO: anyhow
pub fn get_lvs() -> Vec<String> {
    let result = cmd!(
        "lvm",
        "lvs",
        "--reportformat",
        "json",
        "-o",
        "lv_name",
        "vg-pi"
    )
    .stdout_capture()
    .run()
    .expect("could not run lvs");

    let result: LvReport =
        serde_json::from_slice(result.stdout.as_slice()).expect("Could not parse lvm lvs output");

    let mut ret = Vec::new();

    for entry in result.report {
        for actual in entry.lv {
            ret.push(actual.lv_name);
        }
    }

    ret
}

#[derive(Deserialize)]
struct PvReport {
    report: Vec<PvEntry>,
}

#[derive(Deserialize)]
struct PvEntry {
    pv: Vec<ActualPvEntry>,
}

#[derive(Deserialize)]
struct ActualPvEntry {
    pv_name: String,
}

pub fn get_pvs() -> Vec<String> {
    let result = cmd!(
        "lvm",
        "pvs",
        "--reportformat",
        "json",
        "-o",
        "pv_name",
        "--select",
        "vg_name=vg-pi"
    )
    .stdout_capture()
    .run()
    .expect("could not run lvm pvs");

    let result: PvReport =
        serde_json::from_slice(result.stdout.as_slice()).expect("Could not parse lvm pvs output");

    let mut ret = Vec::new();

    for entry in result.report {
        for actual in entry.pv {
            ret.push(actual.pv_name);
        }
    }

    ret
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn lv_parse() {
        let config = r#"{
            "report": [
                {
                    "lv": [
                        {"lv_name":"data"},
                        {"lv_name":"root"}
                    ]
                }
            ]
        }"#;

        let _: LvReport = serde_json::from_str(config).expect("Could not parse test");
    }

    #[test]
    fn pv_parse() {
        let config = r#"{
            "report": [
                {
                    "pv": [
                        {"pv_name":"/dev/sda2"}
                    ]
                }
            ]
        }"#;

        let _: PvReport = serde_json::from_str(config).expect("Coult not parse test");
    }
}
