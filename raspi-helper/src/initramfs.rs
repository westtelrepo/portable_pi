use crate::lvs;
use anyhow::{bail, Context};
use duct::cmd;
use nix::mount::{mount, umount, MsFlags};
use nix::unistd::sync;
use std::env;
use std::ffi::OsString;
use std::fs::{self, create_dir, create_dir_all, read_link, read_to_string, remove_file};
use std::io::ErrorKind;
use std::path::Path;

pub fn initramfs_local_bottom() -> anyhow::Result<()> {
    let rootmnt = &env::var("rootmnt").context("${rootmnt} was not set")?;
    let rootmnt = Path::new(&rootmnt);

    cmd!("chroot", &rootmnt, "modprobe", "vfat")
        .run()
        .context("could not modprobe vfat")?;
    cmd!("chroot", &rootmnt, "modprobe", "overlay")
        .run()
        .context("could not modprobe overlay")?;
    cmd!("chroot", &rootmnt, "modprobe", "nls_cp437")
        .run()
        .context("could not modprobe nls_cp437")?;
    cmd!("chroot", &rootmnt, "modprobe", "nls_ascii")
        .run()
        .context("could not modprobe nls_ascii")?;

    cmd!("vgchange", "-ay")
        .run()
        .context("Could not run vgchange")?;

    // TODO: umount in drop
    mount(
        Some("/dev"),
        &rootmnt.join("dev"),
        Option::<&str>::None,
        MsFlags::MS_BIND,
        Option::<&str>::None,
    )
    .context("could not bind mount dev")?;
    mount(
        Some("/sys"),
        &rootmnt.join("sys"),
        Option::<&str>::None,
        MsFlags::MS_BIND,
        Option::<&str>::None,
    )
    .context("could not bind mount sys")?;

    {
        create_dir_all("/raspifirm").context("could not create /raspifirm")?;

        // TODO: umount in drop
        mount(
            Some("/dev/disk/by-label/RASPIFIRM"),
            "/raspifirm",
            Some("vfat"),
            MsFlags::empty(),
            Option::<&str>::None,
        )
        .context("could not mount RASPIFIRM")?;

        if Path::new("/raspifirm/firstboot").exists() {
            // increase size of second partition
            let pvs = lvs::get_pvs();
            // skip if there are mutliple backing partitions somehow
            if pvs.len() == 1 {
                let partition = &pvs[0];

                println!("partition name: {:?}", partition);

                let disk = {
                    let partname = Path::new(&partition)
                        .file_name()
                        .context("partition name did not have file_name")?;

                    let devicename = read_link(Path::new("/sys/class/block/").join(partname))?;

                    String::from("/dev/")
                        + devicename
                            .parent()
                            .context("bad link in /sys/class/block")?
                            .file_name()
                            .context("bad link in /sys/class/block")?
                            .to_str()
                            .context(
                                "could not convert disk name from /sys/class/block to unicode",
                            )?
                };

                println!("disk name: {:?}", disk);

                cmd!(
                    "chroot",
                    &rootmnt,
                    "sfdisk",
                    "-f",
                    &disk,
                    "-N",
                    "2",
                    "--no-reread"
                )
                .stdin_bytes(",+\n")
                .run()
                .context("sfdisk failed")?;

                cmd!("chroot", &rootmnt, "partprobe", &disk)
                    .run()
                    .context("partprobe failed")?;

                cmd!("lvm", "pvresize", partition)
                    .run()
                    .context("could not run pvresize")?;

                sync();

                remove_file("/raspifirm/firstboot")
                    .context("could not remove /raspifirm/firstboot")?;

                sync();
            }
        }

        if Path::new("/raspifirm/reset.txt").exists() {
            let lvs = lvs::get_lvs();

            if lvs.contains(&String::from("data")) {
                cmd!("lvm", "lvremove", "vg-pi/data", "--yes")
                    .run()
                    .context("could not remove vg-pi/data")?;
                sync();
            }
            remove_file("/raspifirm/reset.txt").context("Could not delete reset.txt")?;
            sync();
        }

        umount("/raspifirm").context("could not umount /raspifirm")?;
    }

    {
        let lvs = lvs::get_lvs();
        if !lvs.contains(&String::from("data")) {
            if lvs.contains(&String::from("data-gen")) {
                cmd!("lvm", "lvremove", "vg-pi/data-gen", "--yes")
                    .run()
                    .context("could not delete data-gen")?;
            }
            // create data
            cmd!("lvm", "lvcreate", "-L", "5G", "-n", "data-gen", "vg-pi", "--yes")
                .run()
                .context("Could not create vg-pi/data")?;
            cmd!(
                "chroot",
                &rootmnt,
                "mke2fs",
                "-t",
                "ext4",
                "-I",
                "256",
                "/dev/vg-pi/data-gen"
            )
            .run()
            .context("Could not format vg-pi/data")?;

            create_dir("/datagen").context("could not create /datagen")?;
            {
                // TODO: umount in drop
                mount(
                    Some("/dev/vg-pi/data-gen"),
                    "/datagen",
                    Some("ext4"),
                    MsFlags::MS_NOATIME,
                    Option::<&str>::None,
                )
                .context("could not mount data-gen")?;
                cmd!(
                    "cp",
                    "-a",
                    rootmnt.join("usr/share/factory/home"),
                    "/datagen"
                )
                .run()
                .context("could not initialize home")?;
                cmd!(
                    "cp",
                    "-a",
                    rootmnt.join("usr/share/factory/root"),
                    "/datagen"
                )
                .run()
                .context("could not initialize /root")?;
                umount("/datagen").context("coult not umount /datagen")?;
            }
            sync();
            cmd!("lvm", "lvrename", "vg-pi", "data-gen", "data")
                .run()
                .context("could not rename vg-pi/data-gen to vg-pi/data")?;
            sync();
        }
    }

    mount(
        Some("/dev/vg-pi/data"),
        &rootmnt.join("data"),
        Some("ext4"),
        MsFlags::MS_NOATIME,
        Option::<&str>::None,
    )
    .context("Could not mount /dev/vg-pi/data")?;

    create_dir_all(rootmnt.join("data/etc/upper"))?;
    create_dir_all(rootmnt.join("data/etc/work"))?;
    create_dir_all(rootmnt.join("data/var/upper"))?;
    create_dir_all(rootmnt.join("data/var/work"))?;

    {
        let mut options = OsString::new();
        options.push("lowerdir=");
        options.push(rootmnt.join("usr/share/factory/etc").as_os_str());
        options.push(",upperdir=");
        options.push(rootmnt.join("data/etc/upper").as_os_str());
        options.push(",workdir=");
        options.push(rootmnt.join("data/etc/work"));

        mount(
            Some("overlay"),
            &rootmnt.join("etc"),
            Some("overlay"),
            MsFlags::empty(),
            Some(options.as_os_str()),
        )
        .context("could not mount etc overlay")?;
    }

    {
        let mut options = OsString::new();
        options.push("lowerdir=");
        options.push(rootmnt.join("usr/share/factory/var").as_os_str());
        options.push(",upperdir=");
        options.push(rootmnt.join("data/var/upper").as_os_str());
        options.push(",workdir=");
        options.push(rootmnt.join("data/var/work"));

        mount(
            Some("overlay"),
            &rootmnt.join("var"),
            Some("overlay"),
            MsFlags::empty(),
            Some(options.as_os_str()),
        )
        .context("could not mount var overlay")?;
    }

    umount(&rootmnt.join("dev")).context("could not umount ${rootmnt}/dev")?;
    umount(&rootmnt.join("sys")).context("could not umount ${rootmnt}/sys")?;

    mount(
        Some("/dev/disk/by-label/RASPIFIRM"),
        &rootmnt.join("raspifirm"),
        Some("vfat"),
        MsFlags::MS_RDONLY | MsFlags::MS_NOATIME,
        Some("umask=077"),
    )
    .context("could not mount RASPIFIRM")?;

    if let Err(e) = config_hostname(rootmnt) {
        eprintln!("error configuring hostname: {:?}", e);
    }

    if let Err(e) = config_timezone(rootmnt) {
        eprintln!("error configuring timezone: {:?}", e);
    }

    Ok(())
}

fn config_hostname(rootmnt: &Path) -> anyhow::Result<()> {
    let hostname = rootmnt.join("raspifirm/data/hostname");
    if hostname.exists() {
        let hostname =
            String::from_utf8(fs::read(hostname).context("could not read data/hostname")?)
                .context("hostname is not valid utf-8")?;
        let hostname = hostname.trim().to_owned();

        fs::write(rootmnt.join("etc/hostname"), hostname.clone() + "\n")
            .context("could not write etc/hostname")?;

        // update exim4 config to use hostname
        fs::write(
            rootmnt.join("etc/mailname"),
            format!("{}.pi.westtel.com\n", hostname),
        )
        .context("could not write etc/mailname")?;

        fs::write(
            rootmnt.join("etc/exim4/update-exim4.conf.conf"),
            format!(
                r#"# DO NOT EDIT! overriden by raspi-helper
dc_eximconfig_configtype='smarthost'
dc_other_hostnames=''
dc_local_interfaces='127.0.0.1 ; ::1'
dc_readhost='{}.pi.westtel.com'
dc_relay_domains=''
dc_minimaldns='false'
dc_relay_nets=''
dc_smarthost='smarthost.wg-management.westtel.com'
CFILEMODE='644'
dc_use_split_config='false'
dc_hide_mailname='true'
dc_mailname_in_oh='true'
dc_localdelivery='mail_spool'
"#,
                hostname
            ),
        )
        .context("could not write etc/exim4/update-exim4.conf.conf")?;
    }

    Ok(())
}

fn config_timezone(rootmnt: &Path) -> anyhow::Result<()> {
    let timezone = rootmnt.join("raspifirm/data/timezone");
    if timezone.exists() {
        let timezone =
            read_to_string(timezone).context("could not read /raspifirm/data/timezone")?;
        let timezone = timezone.trim();

        if rootmnt.join("usr/share/zoneinfo").join(timezone).exists() {
            fs::write(rootmnt.join("etc/timezone"), timezone.to_owned() + "\n")
                .context("could not write /etc/timezone")?;
            match fs::remove_file(rootmnt.join("etc/localtime")) {
                Ok(_) => (),
                Err(e) if e.kind() == ErrorKind::NotFound => (),
                Err(e) => Err(e).context("Could not remove /etc/localtime")?,
            };
            std::os::unix::fs::symlink(
                Path::new("../usr/share/zoneinfo").join(timezone),
                rootmnt.join("etc/localtime"),
            )
            .context("could not create /etc/localtime symlink")?;
        } else {
            bail!("raspi-helper: TIMEZONE INVALID");
        }
    }

    Ok(())
}
