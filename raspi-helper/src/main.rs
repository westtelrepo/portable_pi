use anyhow::{bail, Context};
use cli::{Opt, OptCommand};
use duct::cmd;
use maybe_owned::{MaybeOwnedMut, MaybeOwnedMut::Borrowed, MaybeOwnedMut::Owned};
use nix::fcntl::{flock, FlockArg, OFlag};
use nix::sys::stat::Mode;
use nix::unistd::sync;
use regex::Regex;
use resolve::management_resolv;
use std::collections::HashMap;
use std::ffi::OsStr;
use std::fs::{self, read_dir, read_to_string, remove_dir_all, remove_file};
use std::io::ErrorKind;
use std::os::unix::io::AsRawFd;
use std::path::Path;
use structopt::StructOpt;
use tempfile::tempdir_in;

mod cli;
mod initramfs;
mod lvs;
mod networkd;
mod resolve;

struct RaspifirmRo {
    dir: nix::dir::Dir,
}

impl RaspifirmRo {
    pub fn new() -> anyhow::Result<RaspifirmRo> {
        let dir = nix::dir::Dir::open(
            "/raspifirm",
            OFlag::O_RDONLY | OFlag::O_CLOEXEC,
            Mode::empty(),
        )?;
        flock(dir.as_raw_fd(), FlockArg::LockShared)?;

        Ok(RaspifirmRo { dir })
    }

    pub fn rw_lock(&mut self) -> anyhow::Result<RaspifirmRw> {
        RaspifirmRw::upgrade(&mut self.dir)
    }
}

struct RaspifirmRw<'a> {
    _dir: MaybeOwnedMut<'a, nix::dir::Dir>,
}

impl<'a> RaspifirmRw<'a> {
    pub fn new() -> anyhow::Result<RaspifirmRw<'static>> {
        let dir = nix::dir::Dir::open(
            "/raspifirm",
            OFlag::O_RDONLY | OFlag::O_CLOEXEC,
            Mode::empty(),
        )?;
        flock(dir.as_raw_fd(), FlockArg::LockExclusive)?;
        cmd!("mount", "-o", "remount,rw", "/raspifirm").run()?;

        Ok(RaspifirmRw { _dir: Owned(dir) })
    }

    pub fn upgrade(dir: &'a mut nix::dir::Dir) -> anyhow::Result<RaspifirmRw<'a>> {
        flock(dir.as_raw_fd(), FlockArg::LockExclusive)?;
        cmd!("mount", "-o", "remount,rw", "/raspifirm").run()?;

        Ok(RaspifirmRw {
            _dir: Borrowed(dir),
        })
    }
}

impl<'a> Drop for RaspifirmRw<'a> {
    fn drop(&mut self) {
        match cmd!("mount", "-o", "remount,ro", "/raspifirm").run() {
            Ok(_) => (),
            // ignore error, print
            Err(e) => {
                eprintln!("Could not remount /raspifirm ro!: {:?}", e);
            }
        }
    }
}

fn main() -> anyhow::Result<()> {
    let opt = Opt::from_args();

    match opt.cmd {
        OptCommand::InitramfsLocalBottom => initramfs::initramfs_local_bottom()?,
        OptCommand::InitIwd => initiwd()?,
        OptCommand::InitNetworkd { in_exec_pre } => networkd::initnetworkd(in_exec_pre)?,
        OptCommand::AbUpgrade {
            new_version,
            url,
            new_root_size,
        } => ab_upgrade(&new_version, &url, &new_root_size)?,
        OptCommand::AbGc => ab_gc()?,
        OptCommand::WithRaspifirmLock { command } => with_raspifirm_lock(&command)?,
        OptCommand::AbVersion => println!(
            "AB Version: {}",
            ab_current_version().context("could not get AB version")?
        ),
        OptCommand::FirmwareUpgrade { new_version, url } => upgrade_firmware(&new_version, &url)?,
        OptCommand::ResetNextBoot => reset_next_boot()?,
        OptCommand::WgResolve { hostname } => {
            println!("Result: {}", management_resolv(&hostname)?);
        }
    }

    Ok(())
}

fn reset_next_boot() -> anyhow::Result<()> {
    let _raspifirm = RaspifirmRw::new()?;
    fs::write("/raspifirm/reset.txt", "")?;
    println!("All data will be reset on the next boot! Reboot to reset!");
    Ok(())
}

fn with_raspifirm_lock(command: &[String]) -> anyhow::Result<()> {
    let (program, args) = command.split_first().context("command can not be empty")?;
    let _raspifirm = RaspifirmRw::new()?;
    cmd(program, args).run()?;
    Ok(())
}

fn initiwd() -> anyhow::Result<()> {
    let datapath = Path::new("/raspifirm/data/iwd");
    if datapath.exists() {
        for entry in read_dir("/var/lib/iwd")? {
            let entry = entry?;
            let path = entry.path();
            let filetype = entry.file_type()?;
            let extension = match path.extension() {
                None => continue,
                Some(v) => v,
            };
            if filetype.is_file()
                && (extension == "psk" || extension == "open" || extension == "8021x")
            {
                fs::remove_file(path)?;
            }
        }

        for entry in read_dir(&datapath)? {
            let entry = entry?;
            let path = entry.path();
            let filetype = entry.file_type()?;
            let extension = match path.extension() {
                None => continue,
                Some(v) => v,
            };
            if filetype.is_file()
                && (extension == "psk" || extension == "open" || extension == "8021x")
            {
                let contents = fs::read(path)?;
                fs::write(Path::new("/var/lib/iwd").join(entry.file_name()), contents)?;
            }
        }
    }

    Ok(())
}

fn ab_current_version() -> anyhow::Result<String> {
    // TODO: lazy_static!
    let re = Regex::new(r"^root=/dev/vg-pi/root-(.+)$")?;

    let mut ret = None;

    for arg in read_to_string("/proc/cmdline")?.split_ascii_whitespace() {
        match re.captures(arg) {
            None => continue,
            Some(cap) => match ret {
                None => {
                    ret = Some(
                        cap.get(1)
                            .context("Could not get regex match")?
                            .as_str()
                            .to_owned(),
                    );
                }
                Some(_) => {
                    bail!("Two root= arguments in /proc/cmdline! Can not find old version");
                }
            },
        }
    }

    ret.context("Could not find root= argument, current version undiscoverable")
}

// TODO: use URL type
fn ab_upgrade(new_version: &str, url: &str, new_root_size: &str) -> anyhow::Result<()> {
    let current_version = ab_current_version()?;

    if new_version == current_version {
        bail!("new_version same as current, refusing operation");
    }

    {
        let lvs = lvs::get_lvs();
        let lv_old = "root-".to_owned() + new_version;
        if lvs.contains(&lv_old) {
            cmd!("lvm", "lvremove", "vg-pi/".to_owned() + &lv_old, "--yes").run()?;
        }

        let lv_sync = "root-sync-".to_owned() + new_version;
        if lvs.contains(&lv_sync) {
            cmd!("lvm", "lvremove", "vg-pi/".to_owned() + &lv_sync, "--yes").run()?;
        }
    }

    println!("Creating new root partition...");

    // TODO: configure size and/or figure out size
    cmd!(
        "lvm",
        "lvcreate",
        "-L",
        new_root_size,
        "-n",
        "root-sync-".to_owned() + new_version,
        "vg-pi",
        "--yes"
    )
    .run()?;

    cmd!(
        "casync",
        "extract",
        url.to_owned() + "/" + new_version + "-root.caibx",
        "/dev/vg-pi/root-sync-".to_owned() + new_version,
        "--seed=/dev/vg-pi/root-".to_owned() + &current_version,
        "--verbose"
    )
    .run()?;

    sync();

    println!("Finished downloading new root image, renaming...");

    cmd!(
        "lvm",
        "lvrename",
        "vg-pi",
        "/dev/vg-pi/root-sync-".to_owned() + new_version,
        "/dev/vg-pi/root-".to_owned() + new_version
    )
    .run()?;

    let tmp_firm_dir = tempdir_in("/tmp")?;

    cmd!(
        "casync",
        "extract",
        url.to_owned() + "/" + new_version + "-prefix.caidx",
        tmp_firm_dir.path(),
        "--seed=/raspifirm/prefix/".to_owned() + &current_version,
        "--verbose"
    )
    .run()?;

    println!("Remounting /raspifirm as rw and writing files...");

    {
        let _raspifirm = RaspifirmRw::new()?;

        let new_firm_path = Path::new("/raspifirm/prefix/").join(new_version);

        match remove_dir_all(&new_firm_path) {
            Ok(_) => (),
            // ignore NotFound
            Err(e) if e.kind() == ErrorKind::NotFound => (),
            Err(e) => Err(e).context(format!("could not delete {:?}", &new_firm_path))?,
        };
        cmd!("cp", "-rT", tmp_firm_dir.path(), &new_firm_path).run()?;

        sync();

        println!("Activating new version...");
        fs::copy(
            Path::new("/raspifirm/prefix/")
                .join(new_version)
                .join("config.txt"),
            "/raspifirm/config.txt",
        )?;
    }

    sync();

    println!("New version ready, reboot to apply!");

    Ok(())
}

fn ab_gc() -> anyhow::Result<()> {
    let root_lv_re = Regex::new(r"^root-(.*)$")?;
    let current_version = ab_current_version()?;
    let config_txt = read_to_string("/raspifirm/config.txt")?;

    let lvs = lvs::get_lvs();

    for lv in lvs {
        if let Some(cap) = root_lv_re.captures(&lv) {
            let lv = cap.get(1).context("Broken root_lv_re")?.as_str();
            if lv == current_version || config_txt.contains(lv) {
                continue;
            }

            match cmd!("lvm", "lvremove", "vg-pi/root-".to_owned() + lv, "--yes").run() {
                Ok(_) => (),
                Err(e) => {
                    eprintln!("Could not remove lv for {}: {}", lv, e);
                }
            }
        };
    }

    {
        let _raspifirm = RaspifirmRw::new()?;

        for prefix in read_dir("/raspifirm/prefix")? {
            let prefix = prefix?;
            let file_name = prefix.file_name();
            let version = match file_name.to_str() {
                None => continue, // ignore non-UTF-8 file names
                Some(v) => v,
            };
            if version == current_version || config_txt.contains(version) {
                continue;
            }

            println!("removing {:?}...", prefix.path());
            remove_dir_all(prefix.path())?;
        }
    }

    Ok(())
}

// TODO: what to do if files need to be *removed*
// for now, ignore
fn upgrade_firmware(new_version: &str, url: &str) -> anyhow::Result<()> {
    let seed_dir = tempdir_in("/tmp")?;
    let dl_dir = tempdir_in("/tmp")?;

    let mut raspi = RaspifirmRo::new()?;

    for entry in read_dir("/raspifirm")? {
        let entry = entry?;
        let path = entry.path();
        let extension = path.extension();
        let file_name = match path.file_name() {
            Some(v) => v,
            None => continue,
        };
        if path.is_file()
            && (file_name == OsStr::new("SHASUMS256.txt")
                || file_name == OsStr::new("bootcode.bin")
                || extension == Some(OsStr::new("dat"))
                || extension == Some(OsStr::new("elf")))
        {
            fs::copy(&path, seed_dir.path().join(file_name))?;
        }
    }

    cmd!(
        "casync",
        "extract",
        "--verbose",
        url.to_owned() + "/" + new_version + "-firmware.caidx",
        dl_dir.path(),
        String::from("--seed=") + seed_dir.path().to_str().context("seed_dir is invalid")?
    )
    .run()
    .context("Error downloading new version")?;

    cmd!("sha256sum", "-c", dl_dir.path().join("SHASUMS256.txt"))
        .dir(dl_dir.path())
        .run()
        .context("SHA256 verification failed")?;

    let mut to_upgrade = HashMap::new();

    for entry in read_dir(dl_dir.path())? {
        let entry = entry?;
        let path = entry.path();
        let file_name = path.file_name().context("file didn't have filename!")?;
        if !path.is_file() {
            bail!("Firmware upgrade only understands files");
        }

        let current = match fs::read(Path::new("/raspifirm").join(file_name)) {
            Ok(v) => Some(v),
            Err(e) if e.kind() == ErrorKind::NotFound => None,
            Err(e) => return Err(e.into()),
        };
        let new_version = fs::read(dl_dir.path().join(file_name))?;

        if current.as_ref() != Some(&new_version) {
            to_upgrade.insert(file_name.to_owned(), new_version);
            print!("File {:?} needs upgrading", file_name);
        }
    }

    if !to_upgrade.is_empty() {
        println!("locking /raspifirm to update...");
        {
            let _raspifirm = raspi.rw_lock()?;
            println!("locked /raspifirm");

            println!("writing firmware to drive...");
            for (filename, content) in &to_upgrade {
                let mut staging_file = Path::new("/raspifirm")
                    .join(&filename)
                    .as_os_str()
                    .to_owned();
                staging_file.push(".staging");

                match remove_file(&staging_file) {
                    Ok(_) => (),
                    Err(e) if e.kind() == ErrorKind::NotFound => (),
                    Err(e) => Err(e).context(format!("could not remove {:?}", staging_file))?,
                };

                fs::write(&staging_file, content)?;
            }

            println!("renaming/activating firmware...");
            for filename in to_upgrade.keys() {
                let mut staging_file = Path::new("/raspifirm")
                    .join(&filename)
                    .as_os_str()
                    .to_owned();
                staging_file.push(".staging");

                fs::rename(staging_file, Path::new("/raspifirm").join(&filename))
                    .context(format!("ERROR UPGRADING {:?}!", &filename))?;
            }
        }
        println!("firmware updated, reboot to use!");
    }

    Ok(())
}
