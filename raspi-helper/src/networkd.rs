use crate::resolve::management_resolv;
use anyhow::{bail, Context};
use duct::cmd;
use regex::Regex;
use std::fs::{self, create_dir_all, read_dir, read_to_string};
use std::io::ErrorKind;
use std::path::Path;

pub fn initnetworkd(in_exec_pre: bool) -> anyhow::Result<()> {
    let datapath = Path::new("/raspifirm/data/network");
    if datapath.exists() {
        let wg_service_re = Regex::new(r"__WG_SERVICE__\((?P<hostname>[.a-zA-Z0-9-]+)\)")
            .context("compiling __WG_SERVICE__ regex")?;

        let mut try_restart = false;

        // even though we create this directory with systemd-tmpfiles, it runs *after* systemd-networkd does
        // so we have to create it anyway and tmpfiles will fix the permissions
        create_dir_all("/run/systemd/network").context("Creating /run/systemd/network")?;

        for entry in read_dir(&datapath).context(format!("read_dir({:?})", &datapath))? {
            let entry = entry?;
            let filename = entry.path();
            if filename.is_file() {
                let file = read_to_string(&filename)
                    .context(format!("read_to_string({:?})", &filename))?;

                let mut skip = false;
                let file = wg_service_re.replace_all(&file, |cap: &regex::Captures| {
                    let hostname = cap.name("hostname").unwrap();
                    let hostname = hostname.as_str();

                    match acquire_endpoint(hostname, in_exec_pre) {
                        Ok(v) => v,
                        Err(e) => {
                            eprintln!("error doing DNS for {:?}, skipping: {:?}", hostname, e);
                            skip = true;
                            "".to_string()
                        }
                    }
                });

                if skip {
                    continue;
                }

                let old_file_path = Path::new("/run/systemd/network").join(entry.file_name());
                let old_file = match read_to_string(&old_file_path) {
                    Ok(v) => v,
                    Err(ref e) if e.kind() == ErrorKind::NotFound => "".to_string(),
                    Err(e) => {
                        return Err(e).context(format!("read_to_string({:?})", &old_file_path))
                    }
                };

                if file != old_file {
                    let file_path = Path::new("/run/systemd/network").join(entry.file_name());
                    fs::write(&file_path, file.as_ref())
                        .context(format!("write({:?}", file_path))?;
                    try_restart = true;
                }
            }
        }

        // don't restart if being run from ExecStartPre
        if try_restart && !in_exec_pre {
            cmd!("systemctl", "try-restart", "systemd-networkd")
                .run()
                .context("systemctl try-restart systemd-networkd")?;
        }
    }

    Ok(())
}

fn save_wg_resolv(hostname: &str, result: &str) -> anyhow::Result<()> {
    let cache_dir = Path::new("/var/cache/raspi-helper/wgsrv");
    create_dir_all(&cache_dir).with_context(|| format!("creating dir {:?}", &cache_dir))?;

    let old_result = load_wg_resolv(&hostname)?;

    if old_result.as_deref() == Some(result) {
        return Ok(());
    }

    fs::write(cache_dir.join(hostname), result)
        .with_context(|| format!("writing file {:?}", cache_dir.join(hostname)))?;

    Ok(())
}

fn load_wg_resolv(hostname: &str) -> anyhow::Result<Option<String>> {
    let cache_dir = Path::new("/var/cache/raspi-helper/wgsrv");
    let file = cache_dir.join(hostname);

    match fs::read(&file) {
        Err(e) if e.kind() == ErrorKind::NotFound => Ok(None),
        Err(e) => Err(e).context(format!("Reading {:?}", file)),
        Ok(v) => Ok(Some(String::from_utf8(v)?)),
    }
}

fn acquire_endpoint(hostname: &str, in_exec_pre: bool) -> anyhow::Result<String> {
    // skip trying to resolv if in ExecStartPre, there is no DNS yet
    let endpoint = if !in_exec_pre {
        match management_resolv(&hostname) {
            Err(e) => {
                eprintln!("Could not resolve management endpoint: {:?}", e);
                None
            }
            Ok(v) => {
                match save_wg_resolv(&hostname, &v) {
                    Ok(()) => (),
                    Err(e) => {
                        eprintln!("Could not save result for {:?}: {:?}", &hostname, e)
                    }
                };

                Some(v)
            }
        }
    } else {
        None
    };

    Ok(match endpoint {
        Some(v) => v,
        // load from cache if above did not work
        None => match load_wg_resolv(&hostname) {
            Ok(Some(v)) => v,
            Ok(None) => {
                bail!("Nothing in cache for {:?}, skipping", &hostname);
            }
            Err(e) => {
                return Err(e).context(format!("Error loading {:?} from cache", &hostname));
            }
        },
    })
}
