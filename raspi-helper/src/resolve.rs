use anyhow::{bail, Context};
use serde::Deserialize;
use std::convert::TryInto;
use std::net::{IpAddr, SocketAddr};
use zbus::{dbus_proxy, Connection};
use zvariant_derive::Type;

#[derive(Debug, Deserialize, Type)]
pub struct SrvAddress {
    pub ifindex: i32,
    pub family: i32,
    pub address: Vec<u8>,
}
#[derive(Debug, Deserialize, Type)]
pub struct SrvData {
    pub priority: u16,
    pub weight: u16,
    pub port: u16,
    pub hostname: String,
    pub addresses: Vec<SrvAddress>,
    pub canonical_hostname: String,
}

#[derive(Debug, Deserialize, Type)]
pub struct SrvInfo {
    pub srv_data: Vec<SrvData>,
    pub txt_data: Vec<Vec<u8>>,
    pub canonical_name: String,
    pub canonical_type: String,
    pub canonical_domain: String,
    pub flags: u64,
}

#[dbus_proxy(
    interface = "org.freedesktop.resolve1.Manager",
    default_service = "org.freedesktop.resolve1",
    default_path = "/org/freedesktop/resolve1"
)]
pub trait Resolve {
    fn resolve_service(
        &self,
        ifindex: i32,
        name: &str,
        srv_type: &str,
        domain: &str,
        family: i32,
        flags: u64,
    ) -> zbus::Result<SrvInfo>;
}

pub fn management_resolv(hostname: &str) -> anyhow::Result<String> {
    let system = Connection::new_system()?;
    let resolv = ResolveProxy::new(&system)?;

    let results = resolv.resolve_service(0, "", "_wireguard._udp", &hostname, 0, 0)?;

    // TODO: sort by priority instead of just taking the first result
    for result in results.srv_data {
        let address = result.addresses.get(0).context("No Address")?;
        let addr = match address.family {
            libc::AF_INET => {
                let addr: [u8; 4] = address.address.as_slice().try_into()?;
                IpAddr::from(addr)
            }
            libc::AF_INET6 => {
                let addr: [u8; 16] = address.address.as_slice().try_into()?;
                IpAddr::from(addr)
            }
            _ => continue,
        };

        let socket = SocketAddr::new(addr, result.port);

        return Ok(socket.to_string());
    }

    bail!("Could not find address");
}
