include!("src/cli.rs");

use std::fs::create_dir_all;

fn main() {
    create_dir_all("./completions").unwrap();

    Opt::clap().gen_completions(
        "raspi-helper",
        structopt::clap::Shell::Bash,
        "./completions",
    );
}
