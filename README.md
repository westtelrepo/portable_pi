# Introduction

Developer documentation is contained in respective folders.

Documentation is also on the WestTel wiki.

# Build

Install vagrant [https://www.vagrantup.com/docs/installation] and VirtualBox.

`vagrant plugin install vagrant-vbguest`

`vagrant up`

`vagrant ssh`

`cd /vagrant`

`cross build --target aarch64-unknown-linux-musl --release`

`cd /vagrant/pi`

# go have some tea, this will take a long time the first time
# build the Raspberry Pi images and Casync repos
`sudo ./build.sh`

# update casync server

`cd /vagrant`

`rsync -av casync_data/ wg-management.lan:/srv/casync/`

`ssh wg-management.lan`

`cd /srv/casync`

Only needed if you delete any `*.cai*` files from `/srv/casync`:
`casync gc *.cai*`

# build the on-site WireGuard VM

`cd /vagrant/server`

`sudo ./build.sh`

# management network

IPv6 ULA

Global ID: 975e16a609

Site ID 0000: PIs

fd97:5e16:a609:0000:xxxx:xxxx:xxxx:xxxx/64

Site ID 0001: Management services (DNS, casync, etc)

SRV records at wg-management-endpoint.westtel.com

NOTE: this address, and the DNS names the SRV records point to, may not be
wg-management.westtel.com or any subdomain!

# Upgrading images notes

`/pi/build.sh` uses the snapshot.debian.org repository, the dates will need to be updated.
Warning: the 5.10 kernel from buster-backports doesn't work right now (2021-04-02).

`/pi/iwd.sh` uses git pinning that needs to be updated occasionally

`/server/build.sh` does not use any pinning at the moment

# wpinger install (update)

use `cross build --target aarch64-unknown-linux-musl --release` to build a new wpinger release (in the wpinger repository)

Copy it as the wpinger-aarch64-unknown-linux file in this repository.

Use `aarch64-linux-gnu-strip /vagrant/wpinger-aarch64-unknown-linux-musl` before committing
to make it smaller.
