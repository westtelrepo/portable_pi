#!/bin/bash

set -e

source ../shared.sh

export SOURCE_DATE_EPOCH=0

# export REPO_ROOT=http://10.0.2.2:3142/snapshot
# export DEBIAN_REPO=$REPO_ROOT/debian/20201201T031901Z/
# export SECURITY_REPO=$REPO_ROOT/debian-security/20201201T010307Z/
# export CHECK_VALID_UNTIL=" [check-valid-until=no]"

export REPO_ROOT=http://127.0.0.1:3142
export DEBIAN_REPO=$REPO_ROOT/debian
export SECURITY_REPO=$REPO_ROOT/debian-security

# export REPO_ROOT=https://deb.debian.org
# export DEBIAN_REPO=$REPO_ROOT/debian
# export SECURITY_REPO=$REPO_ROOT/debian-security

export IMAGE=/root/serverimage.raw
export BUILDDIR=/root/buildserver

export VERSION="$(date +%Y%m%d)-$(</dev/urandom tr -dc 'a-zA-Z0-9' | head -c 8)"

function cleanup {
    echo "cleanup..."
    umount $BUILDDIR/efi || true
    umount $BUILDDIR || true
    vgchange -an wg-server || true
    kpartx -dsv $IMAGE || true
}
trap cleanup EXIT

mkdir -p $BUILDDIR

rm -f $IMAGE

# WARNING: if you change this size you will have to change the .ovf file (generated at bottom
# of this script)
qemu-img create -f raw $IMAGE 20G

echo "parted..."
parted -s $IMAGE mklabel gpt
parted -s -a optimal $IMAGE mkpart "" fat32 0% 512M
parted -s $IMAGE set 1 boot on
parted -s $IMAGE set 1 esp on
parted -s -a optimal $IMAGE mkpart "" ext4 512M 100%
parted -s -a optimal $IMAGE set 2 lvm on

echo "kpartx..."
kpartx -asv $IMAGE

mkfs -t vfat /dev/mapper/loop0p1
vgcreate -y wg-server /dev/mapper/loop0p2

lvcreate -L 1G -n swap wg-server
lvcreate -l 100%FREE -n root wg-server

mkswap /dev/wg-server/swap
mkfs -t ext4 /dev/wg-server/root

# discard: use TRIM to "zero-out" freed blocks, making the final image smaller
mount -o discard /dev/wg-server/root $BUILDDIR
mkdir -p $BUILDDIR/efi/
mount -o discard /dev/mapper/loop0p1 $BUILDDIR/efi

if [ ! -d /root/serverdeb.cache ]; then
    echo "debootstrap"
    debootstrap buster $BUILDDIR $DEBIAN_REPO

    # [check-valid-until=no] when using snapshot
    cat <<EOF > $BUILDDIR/etc/apt/sources.list
deb$CHECK_VALID_UNTIL $DEBIAN_REPO buster main contrib non-free
deb$CHECK_VALID_UNTIL $SECURITY_REPO buster/updates main contrib non-free
deb$CHECK_VALID_UNTIL $DEBIAN_REPO buster-backports main contrib non-free
EOF

    cat <<EOF > $BUILDDIR/etc/locale.gen
en_US.UTF-8 UTF-8
EOF

    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR apt-get update
    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR -E DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::="--force-confold" -y install \
        bash-completion bc bzip2 dbus dc dnsutils ed file ftp gawk htop iftop info iotop less \
        libpam-systemd mlocate moreutils parted rsync screen sysstat telnet unzip uuid-runtime \
        vim-nox whois jq curl sudo lz4 libreadline7 man-db acl locales iputils-tracepath traceroute \
        bsd-mailx mutt strace conntrack debconf-utils
    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR debconf-set-selections <<EOF
wireshark-common        wireshark-common/install-setuid boolean true
EOF
    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR -E DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::="--force-confold" -y install \
        --no-install-recommends tcpdump wireshark-common
    cp -a $BUILDDIR /root/serverdeb.cache
else
    echo "copying cached"
    cp -aT /root/serverdeb.cache $BUILDDIR
fi

# remove, not using
rm -f $BUILDDIR/etc/{subuid,subuid-,subgid,subgid-}

# setup coredumps
mkdir -p $BUILDDIR/etc/tmpfiles.d
cat <<EOF > $BUILDDIR/etc/tmpfiles.d/00_override-coredump.conf
# 2755 has sgid bit set, which allows adm group to read the contents
# of ALL coredumps.
d /var/lib/systemd/coredump 2755 root adm 31d
EOF

mkdir -p $BUILDDIR/etc/monit/conf.d
cat <<EOF > $BUILDDIR/etc/monit/conf.d/coredump
check program coredump with path "/bin/bash -c 'ls /var/lib/systemd/coredump; exit $(ls /var/lib/systemd/coredump | wc -l)'" timeout 10 seconds
	if changed status then alert
	if status != 0 then alert
EOF

# don't try resuming, we don't need it and it takes 30 seconds longer to boot
mkdir -p $BUILDDIR/etc/initramfs-tools/conf.d/
cat <<EOF > $BUILDDIR/etc/initramfs-tools/conf.d/resume
RESUME=none
EOF

# setup system configuration
mkdir -p $BUILDDIR/etc/systemd/
cat <<EOF > $BUILDDIR/etc/systemd/system.conf
[Manager]
DefaultLimitCORE=infinity
DefaultMemoryAccounting=yes
DefaultCPUAccounting=yes
DefaultIOAccounting=yes
EOF
cat <<EOF > $BUILDDIR/etc/systemd/journald.conf
[Journal]
SystemMaxUse=1G
EOF
cat <<EOF > $BUILDDIR/etc/systemd/timesyncd.conf
[Time]
FallbackNTP=0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org
EOF

mkdir -p $BUILDDIR/efi/loader/entries
cat <<EOF > $BUILDDIR/efi/loader/loader.conf
default debian-*
timeout 5
editor no
EOF

# note: for whatever reason, VMware UEFI doesn't support the +1.conf rename to .conf mechanism
mkdir -p $BUILDDIR/etc/initramfs/post-update.d
cat <<'BIGEOF' > $BUILDDIR/etc/initramfs/post-update.d/zz-systemd-boot
#!/bin/bash

for i in /boot/vmlinuz-*; do
    version=${i##/boot/vmlinuz-}
    if [ -f /boot/vmlinuz-$version ] && [ -f /boot/initrd.img-$version ]; then
        # skip if the files are the same
        if ! cmp -s /boot/vmlinuz-$version /efi/EFI/Debian/$version/vmlinuz-$version || \
            ! cmp -s /boot/initrd.img-$version /efi/EFI/Debian/$version/initrd.img-$version; then
            echo "Updating kernel $version into EFI"
            rm -f /efi/loader/entries/debian-$version*.conf
            sync --file-system /efi/
            mkdir -p /efi/EFI/Debian/$version
            cp /boot/vmlinuz-$version /efi/EFI/Debian/$version/vmlinuz-$version
            cp /boot/initrd.img-$version /efi/EFI/Debian/$version/initrd.img-$version
            sync --file-system /efi/
            cat <<EOF > /efi/loader/entries/debian-$version.conf
title Debian GNU/Linux 10 (buster)
version $version
linux /EFI/Debian/$version/vmlinuz-$version
initrd /EFI/Debian/$version/initrd.img-$version
options root=/dev/wg-server/root ro quiet net.ifnames=0
EOF
            sync --file-system /efi/
        fi
    fi
done
for i in /efi/EFI/Debian/*; do
    version=${i##/efi/EFI/Debian/}
    if [ ! -f /boot/vmlinuz-$version ]; then
        echo "Removing kernel verion $version from EFI"
        rm -rf /efi/loader/entries/debian-$version*.conf
        sync --file-system /efi/
        rm -rf /efi/EFI/Debian/$version
        sync --file-system /efi/
    fi
done
BIGEOF
chmod +x $BUILDDIR/etc/initramfs/post-update.d/zz-systemd-boot

mkdir -p $BUILDDIR/datadisk1
ln -s /var/lib/systemd/coredump $BUILDDIR/datadisk1/coredump

mkdir -p $BUILDDIR/etc/systemd/network

cat <<EOF > $BUILDDIR/etc/systemd/network/eth0.network
[Match]
Name = eth0
[Network]
Address =
Gateway =
DNS =
NTP =

IPForward = yes
IPv4ProxyARP = yes
EOF

cat <<EOF > $BUILDDIR/etc/systemd/network/wgpi.netdev
Name = wgpi
Kind = wireguard
Description = wireguard server for pi

[WireGuard]
# NOTE: create a key and put it here
PrivateKey =
# change if needed
ListenPort = 51820

# NOTE: this isn't real, replace
[WireGuardPeer]
PublicKey = 9oPJk9J93KK8qZcUNdmBzw34L2IQXjAzCocL8DeHujA=
AllowedIPs = 192.168.59.50/32, 192.168.59.107/32

# NOTE: add others
EOF

cat <<EOF > $BUILDDIR/etc/systemd/network/wgpi.network
[Match]
Name = wgpi

[Network]
IPForward = yes
IPv4ProxyARP = yes

# [Route]
# Destination = 192.168.59.50/32
EOF

echo "wgserver-$VERSION" > $BUILDDIR/etc/hostname

export EXTRAPACKAGES="ssh parted wireguard apt-file bsd-mailx \
    command-not-found cruft debian-security-support debsums \
    systemd-coredump monit exim4 ufw open-vm-tools"

systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR apt-get update
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR -E DEBIAN_FRONTEND=noninteractive apt-get -y install linux-image-amd64 lvm2 $EXTRAPACKAGES
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR bootctl install
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR systemctl enable systemd-networkd systemd-resolved

systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR apt-file update
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR update-command-not-found

systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR ufw allow in on eth0 from any to any port 22 proto tcp comment "SSH"
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR ufw allow in on eth0 from any to any port 2812 proto tcp comment "Monit"
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR ufw allow in on eth0 from any to any port 51820 proto udp comment "WireGuard"
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR ufw route allow in on eth0 out on wgpi comment "Outbound to Pi"

do_systemd_network_perms

# monit setup
do_monit

cat <<EOF > $BUILDDIR/etc/monit/conf.d/fs
check filesystem rootfs with path /dev/wg-server/root
    if space > 80% for 60 cycles then alert
    if inode usage > 80% for 60 cycles then alert

check filesystem efifs with path /dev/disk/by-partuuid/$(findmnt -o partuuid --noheading /dev/mapper/loop0p1)
    if space > 80% for 60 cycles then alert
    if inode usage > 80% for 60 cycles then alert
EOF

# create westtel user
# password created with `mkpasswd --method=sha512crypt`
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR useradd westtel --groups adm,systemd-journal,sudo --create-home --user-group --shell /bin/bash --password '$6$d2lVzUTwwz8MBGGx$p6tCOiJlvV/725kJpmOaRAa8NABQTgOqIwmbmnInDxAg34CFMQzZZk0xu.mDcQqYhRDTGPMVrnHKfysUOuZ7j/'

mkdir -p $BUILDDIR/efi/EFI/recovery
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR cp /{vmlinuz,initrd.img} /efi/EFI/recovery
cat <<EOF > $BUILDDIR/efi/loader/entries/00-recovery.conf
title Recovery for Debian GNU/Linux 10 (buster)
linux /EFI/recovery/vmlinuz
initrd /EFI/recovery/initrd.img
options root=/dev/wg-server/root ro quiet net.ifnames=0
EOF

# allow root logins locally with no password
# sed -i 's,root:[^:]*:,root::,' $BUILDDIR/etc/shadow

cat <<EOF > $BUILDDIR/etc/fstab
/dev/wg-server/root / ext4 defaults,errors=remount-ro 0 1
PARTUUID=$(findmnt -o partuuid --noheading /dev/mapper/loop0p1) /efi vfat umask=0077 0 0
/dev/wg-server/swap none swap defaults 0 0
tmpfs /tmp tmpfs strictatime,nosuid,nodev,size=512M 0 0
EOF

do_ssh

rm -rf $BUILDDIR/var/log/journal
mkdir $BUILDDIR/var/log/journal

# use systemd-resolved
rm $BUILDDIR/etc/resolv.conf
ln -s /run/systemd/resolve/stub-resolv.conf $BUILDDIR/etc/resolv.conf

rm -rf $BUILDDIR/var/log/journal
mkdir $BUILDDIR/var/log/journal

# clean up apt info
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR apt-get clean
rm -rf $BUILDDIR/var/lib/apt/lists

# use deb.debian.org for final image
cat <<EOF > $BUILDDIR/etc/apt/sources.list
deb https://deb.debian.org/debian buster main contrib non-free
deb https://deb.debian.org/debian-security buster/updates main contrib non-free
deb https://deb.debian.org/debian buster-backports main contrib non-free
EOF

# warning
cat <<EOF > $BUILDDIR/etc/network/interfaces
# DO NOT USE! network configuration is in /etc/systemd/network

# interfaces(5) file used by ifup(8) and ifdown(8)
# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d
EOF

# last step, initialize the updatedb database
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR updatedb

rm -f $BUILDDIR/etc/machine-id $BUILDDIR/var/lib/dbus/machine-id
touch $BUILDDIR/etc/machine-id

# add mtab
ln -s ../proc/self/mounts $BUILDDIR/etc/mtab

# empty dev
rm -rf $BUILDDIR/dev
mkdir $BUILDDIR/dev

# clean up var
rm -rf $BUILDDIR/var/log/{chrony,monit.log} $BUILDDIR/var/lib/{chrony,dbus}

# set clock to now
mkdir -p $BUILDDIR/var/lib/systemd/timesync
touch $BUILDDIR/var/lib/systemd/timesync/clock
# systemd StateDirectory= will fix above permissions

cleanup
trap - EXIT

# convert final disk image to OVA
rm -rf /root/ova
mkdir /root/ova
# default options don't work for VMware
qemu-img convert -f raw $IMAGE -O vmdk \
    -o adapter_type=lsilogic,subformat=streamOptimized,compat6 /root/ova/disk.vmdk

# this ovf was gotten from creating a VM in vCenter and then exporting it and editing it accordingly
cat <<EOF > /root/ova/PortablePiServer-$VERSION.ovf
<?xml version="1.0" encoding="UTF-8"?>
<Envelope vmw:buildId="build-2583090" xmlns="http://schemas.dmtf.org/ovf/envelope/1" xmlns:cim="http://schemas.dmtf.org/wbem/wscim/1/common" xmlns:ovf="http://schemas.dmtf.org/ovf/envelope/1" xmlns:rasd="http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_ResourceAllocationSettingData" xmlns:vmw="http://www.vmware.com/schema/ovf" xmlns:vssd="http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_VirtualSystemSettingData" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <References>
    <File ovf:href="disk.vmdk" ovf:id="file1" ovf:size="$(stat /root/ova/disk.vmdk --format="%s")" />
  </References>
  <DiskSection>
    <Info>Virtual disk information</Info>
    <Disk ovf:capacity="20" ovf:capacityAllocationUnits="byte * 2^30" ovf:diskId="vmdisk1" ovf:fileRef="file1" ovf:format="http://www.vmware.com/interfaces/specifications/vmdk.html#streamOptimized" ovf:populatedSize="0" />
  </DiskSection>
  <NetworkSection>
    <Info>The list of logical networks</Info>
    <Network ovf:name="VM Network">
      <Description>The VM Network network</Description>
    </Network>
  </NetworkSection>
  <VirtualSystem ovf:id="Portable Pi Server">
    <Info>A virtual machine</Info>
    <Name>Portable Pi Server</Name>
    <OperatingSystemSection ovf:id="96" ovf:version="6" vmw:osType="debian6_64Guest">
      <Info>The kind of installed guest operating system</Info>
    </OperatingSystemSection>
    <VirtualHardwareSection>
      <Info>Virtual hardware requirements</Info>
      <System>
        <vssd:ElementName>Virtual Hardware Family</vssd:ElementName>
        <vssd:InstanceID>0</vssd:InstanceID>
        <vssd:VirtualSystemIdentifier>Portable Pi Server</vssd:VirtualSystemIdentifier>
        <vssd:VirtualSystemType>vmx-08</vssd:VirtualSystemType>
      </System>
      <Item>
        <rasd:AllocationUnits>hertz * 10^6</rasd:AllocationUnits>
        <rasd:Description>Number of Virtual CPUs</rasd:Description>
        <rasd:ElementName>1 virtual CPU(s)</rasd:ElementName>
        <rasd:InstanceID>1</rasd:InstanceID>
        <rasd:ResourceType>3</rasd:ResourceType>
        <rasd:VirtualQuantity>1</rasd:VirtualQuantity>
      </Item>
      <Item>
        <rasd:AllocationUnits>byte * 2^20</rasd:AllocationUnits>
        <rasd:Description>Memory Size</rasd:Description>
        <rasd:ElementName>256MB of memory</rasd:ElementName>
        <rasd:InstanceID>2</rasd:InstanceID>
        <rasd:ResourceType>4</rasd:ResourceType>
        <rasd:VirtualQuantity>256</rasd:VirtualQuantity>
      </Item>
      <Item>
        <rasd:Address>0</rasd:Address>
        <rasd:Description>SCSI Controller</rasd:Description>
        <rasd:ElementName>SCSI Controller 0</rasd:ElementName>
        <rasd:InstanceID>3</rasd:InstanceID>
        <rasd:ResourceSubType>lsilogic</rasd:ResourceSubType>
        <rasd:ResourceType>6</rasd:ResourceType>
      </Item>
      <Item>
        <rasd:Address>1</rasd:Address>
        <rasd:Description>IDE Controller</rasd:Description>
        <rasd:ElementName>VirtualIDEController 1</rasd:ElementName>
        <rasd:InstanceID>4</rasd:InstanceID>
        <rasd:ResourceType>5</rasd:ResourceType>
      </Item>
      <Item>
        <rasd:Address>0</rasd:Address>
        <rasd:Description>IDE Controller</rasd:Description>
        <rasd:ElementName>VirtualIDEController 0</rasd:ElementName>
        <rasd:InstanceID>5</rasd:InstanceID>
        <rasd:ResourceType>5</rasd:ResourceType>
      </Item>
      <Item ovf:required="false">
        <rasd:AutomaticAllocation>false</rasd:AutomaticAllocation>
        <rasd:ElementName>VirtualVideoCard</rasd:ElementName>
        <rasd:InstanceID>6</rasd:InstanceID>
        <rasd:ResourceType>24</rasd:ResourceType>
        <vmw:Config ovf:required="false" vmw:key="enable3DSupport" vmw:value="false" />
        <vmw:Config ovf:required="false" vmw:key="enableMPTSupport" vmw:value="false" />
        <vmw:Config ovf:required="false" vmw:key="use3dRenderer" vmw:value="automatic" />
        <vmw:Config ovf:required="false" vmw:key="useAutoDetect" vmw:value="false" />
        <vmw:Config ovf:required="false" vmw:key="videoRamSizeInKB" vmw:value="8192" />
      </Item>
      <Item ovf:required="false">
        <rasd:AutomaticAllocation>false</rasd:AutomaticAllocation>
        <rasd:ElementName>VirtualVMCIDevice</rasd:ElementName>
        <rasd:InstanceID>7</rasd:InstanceID>
        <rasd:ResourceSubType>vmware.vmci</rasd:ResourceSubType>
        <rasd:ResourceType>1</rasd:ResourceType>
        <vmw:Config ovf:required="false" vmw:key="allowUnrestrictedCommunication" vmw:value="false" />
      </Item>
      <Item>
        <rasd:AddressOnParent>0</rasd:AddressOnParent>
        <rasd:ElementName>Hard Disk 1</rasd:ElementName>
        <rasd:HostResource>ovf:/disk/vmdisk1</rasd:HostResource>
        <rasd:InstanceID>8</rasd:InstanceID>
        <rasd:Parent>3</rasd:Parent>
        <rasd:ResourceType>17</rasd:ResourceType>
        <vmw:Config ovf:required="false" vmw:key="backing.writeThrough" vmw:value="false" />
      </Item>
      <Item ovf:required="false">
        <rasd:AddressOnParent>0</rasd:AddressOnParent>
        <rasd:AutomaticAllocation>false</rasd:AutomaticAllocation>
        <rasd:ElementName>CD-ROM 1</rasd:ElementName>
        <rasd:InstanceID>9</rasd:InstanceID>
        <rasd:Parent>4</rasd:Parent>
        <rasd:ResourceSubType>vmware.cdrom.remotepassthrough</rasd:ResourceSubType>
        <rasd:ResourceType>15</rasd:ResourceType>
        <vmw:Config ovf:required="false" vmw:key="backing.exclusive" vmw:value="false" />
        <vmw:Config ovf:required="false" vmw:key="connectable.allowGuestControl" vmw:value="true" />
      </Item>
      <Item>
        <rasd:AddressOnParent>7</rasd:AddressOnParent>
        <rasd:AutomaticAllocation>true</rasd:AutomaticAllocation>
        <rasd:Connection>VM Network</rasd:Connection>
        <rasd:Description>E1000 ethernet adapter on "VM Network"</rasd:Description>
        <rasd:ElementName>Ethernet 1</rasd:ElementName>
        <rasd:InstanceID>10</rasd:InstanceID>
        <rasd:ResourceSubType>E1000</rasd:ResourceSubType>
        <rasd:ResourceType>10</rasd:ResourceType>
        <vmw:Config ovf:required="false" vmw:key="connectable.allowGuestControl" vmw:value="true" />
        <vmw:Config ovf:required="false" vmw:key="wakeOnLanEnabled" vmw:value="true" />
      </Item>
      <Item ovf:required="false">
        <rasd:AddressOnParent>0</rasd:AddressOnParent>
        <rasd:AutomaticAllocation>false</rasd:AutomaticAllocation>
        <rasd:Description>Floppy Drive</rasd:Description>
        <rasd:ElementName>Floppy 1</rasd:ElementName>
        <rasd:InstanceID>11</rasd:InstanceID>
        <rasd:ResourceSubType>vmware.floppy.remotedevice</rasd:ResourceSubType>
        <rasd:ResourceType>14</rasd:ResourceType>
        <vmw:Config ovf:required="false" vmw:key="connectable.allowGuestControl" vmw:value="true" />
      </Item>
      <vmw:Config ovf:required="false" vmw:key="cpuHotAddEnabled" vmw:value="false" />
      <vmw:Config ovf:required="false" vmw:key="cpuHotRemoveEnabled" vmw:value="false" />
      <vmw:Config ovf:required="false" vmw:key="firmware" vmw:value="efi" />
      <vmw:Config ovf:required="false" vmw:key="virtualICH7MPresent" vmw:value="false" />
      <vmw:Config ovf:required="false" vmw:key="virtualSMCPresent" vmw:value="false" />
      <vmw:Config ovf:required="false" vmw:key="memoryHotAddEnabled" vmw:value="false" />
      <vmw:Config ovf:required="false" vmw:key="nestedHVEnabled" vmw:value="false" />
      <vmw:Config ovf:required="false" vmw:key="powerOpInfo.powerOffType" vmw:value="soft" />
      <vmw:Config ovf:required="false" vmw:key="powerOpInfo.resetType" vmw:value="soft" />
      <vmw:Config ovf:required="false" vmw:key="powerOpInfo.standbyAction" vmw:value="powerOnSuspend" />
      <vmw:Config ovf:required="false" vmw:key="powerOpInfo.suspendType" vmw:value="hard" />
      <vmw:Config ovf:required="false" vmw:key="tools.afterPowerOn" vmw:value="true" />
      <vmw:Config ovf:required="false" vmw:key="tools.afterResume" vmw:value="true" />
      <vmw:Config ovf:required="false" vmw:key="tools.beforeGuestShutdown" vmw:value="true" />
      <vmw:Config ovf:required="false" vmw:key="tools.beforeGuestStandby" vmw:value="true" />
      <vmw:Config ovf:required="false" vmw:key="tools.syncTimeWithHost" vmw:value="false" />
      <vmw:Config ovf:required="false" vmw:key="tools.toolsUpgradePolicy" vmw:value="manual" />
    </VirtualHardwareSection>
  </VirtualSystem>
</Envelope>
EOF

cat <<EOF > /root/ova/PortablePiServer-$VERSION.mf
SHA1(disk.vmdk)= $(sha1sum /root/ova/disk.vmdk | cut -c1-40)
SHA1(PortablePiServer-$VERSION.ovf)= $(sha1sum /root/ova/PortablePiServer-$VERSION.ovf | cut -c1-40)
EOF

echo "tar'ing into OVA"

# VMware requires that the .ovf file is the first in the archive
tar -cf ./PortablePiServer-$VERSION.ova -C /root/ova PortablePiServer-$VERSION.ovf \
    PortablePiServer-$VERSION.mf disk.vmdk
