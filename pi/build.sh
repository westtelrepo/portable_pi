#!/bin/bash

set -e

# TODO: check /etc/passwd, shadow, group, gshadow, subuid, subgid, preinitialize it and specify IDs
# TODO: raspi-helper gen-monit-conf, run every time (# WARNING: changes will be overriden every monit restart!)

# config.txt os_prefix notes:
# only *.dtb, cmdline.txt, initrd and vmlinuz can go into prefix (config.txt for recovery)
# config.txt copied into /
# bootcode.bin, *.dat, *.elf need to be in /

# mail notes (TODO):
# general: mail sent by smarthost, no local mail
# system mail name: have it be blank?
# IP addresses to listen to: leave as localhost/127.0.0.1/::1/etc
# other destinations for which mail is accepted: leave it blank
# visible domain name for local users: leave it blank
# outgoing smarthost: smarthost.wg-management.westtel.com
# keep queries minimal: no
# split configuration into small files: no
# root and postmaster mail recipient: leave it blank

source ../shared.sh

export SOURCE_DATE_EPOCH=0

# bullseye or buster
# NOTE: bullseye doesn't work at all right now
export DEBIAN_RELEASE=buster

# if you change these you have to `sudo rm -rf /root/arm64deb.cache`
export REPO_ROOT=http://127.0.0.1:3142
export DEBIAN_REPO=$REPO_ROOT/snapshot/debian/20210428T143828Z/
export SECURITY_REPO=$REPO_ROOT/snapshot/debian-security/20210428T015032Z/
# BEWARE WHEN UPDATING, kernel 5.10.0-0.bpo.4 and 5.10.0-0.bpo.5 don't work
export BACKPORTS_REPO=$REPO_ROOT/snapshot/debian/20210331T212131Z/
export CHECK_VALID_UNTIL=" [check-valid-until=no]"

export IMAGE=/root/tmp/raspi_4.img
export BUILDDIR=/root/build
export CASYNCDIR=/vagrant/casync_data

export VERSION="$(date +%Y%m%d)-$(</dev/urandom tr -dc 'a-zA-Z0-9' | head -c 8)"

function cleanup {
    # bash only allows one "trap EXIT", so put all cleanup in the same function
    # because of `set -e`, use `|| true` to ignore failures (which most of the time is
    # going to be "we haven't mounted it yet")
    echo "cleanup..."
    umount $BUILDDIR/boot/firmware || true
    vgchange -an vg-pi || true
    kpartx -dsv $IMAGE || true
    rm -rf $BUILDDIR || true
}
cleanup
trap cleanup EXIT

mkdir -p /root/tmp

rm -f $IMAGE

qemu-img create -f raw $IMAGE 700M

parted -s $IMAGE mklabel msdos
parted -s $IMAGE mkpart primary fat32 0% 300M
parted -s $IMAGE mkpart primary ext4 300M 100%
parted -s $IMAGE set 2 lvm on

# TODO: don't hardcode loop0p1 and loop0p2!
kpartx -asv $IMAGE

rm -rf $BUILDDIR
mkdir -p $BUILDDIR
mkfs -t vfat -n RASPIFIRM /dev/mapper/loop0p1
vgcreate -y vg-pi /dev/mapper/loop0p2

function install_packages {
    while true; do
        set +e
        systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR -E DEBIAN_FRONTEND=noninteractive apt-get \
            -o Dpkg::Options::="--force-confold" -y install --download-only "$@"
        retVal=$?
        set -e
        if [ $retVal -ne 100 ]; then
            break;
        fi
        echo "error downloading packages, wait and try again..."
        sleep 15
    done

    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR -E DEBIAN_FRONTEND=noninteractive apt-get \
        -o Dpkg::Options::="--force-confold" -y install "$@"
}

function update_packages {
    while true; do
        set +e
        systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR apt-get update
        retVal=$?
        set -e
        if [ $retVal -ne 100 ]; then
            break
        fi
        echo "error updating packages, wait and try again..."
        sleep 15
    done
}

if [ ! -d /root/arm64deb.cache ]; then
    mkdir -p $BUILDDIR/etc
    cp ./etc/{group,gshadow,passwd,shadow} $BUILDDIR/etc/
    chmod u=rw,go=r $BUILDDIR/etc/{passwd,group}
    chmod u=rw,g=r,o= $BUILDDIR/etc/{shadow,gshadow}
    chgrp shadow $BUILDDIR/etc/{shadow,gshadow}

    echo "debootstrap"
    while true; do
        rm -rf /tmp/debootstrap
        cp -a $BUILDDIR /tmp/debootstrap
        set +e
        debootstrap --foreign --arch=arm64 $DEBIAN_RELEASE /tmp/debootstrap $DEBIAN_REPO
        retVal=$?
        set -e
        if [ $retVal -eq 0 ]; then
            rm -rf $BUILDDIR
            cp -aT /tmp/debootstrap $BUILDDIR
            rm -rf /tmp/debootstrap
            break
        fi
        echo "debootstrap returned error, trying again"
        sleep 15
    done
    echo "debootstrap second stage"
    chroot $BUILDDIR /debootstrap/debootstrap --second-stage

    echo "APT::Default-Release \"$DEBIAN_RELEASE\";" > $BUILDDIR/etc/apt/apt.conf
if [ $DEBIAN_RELEASE = "buster" ]; then
    cat <<EOF > $BUILDDIR/etc/apt/sources.list
deb$CHECK_VALID_UNTIL $DEBIAN_REPO buster main contrib non-free
deb$CHECK_VALID_UNTIL $SECURITY_REPO buster/updates main contrib non-free
deb$CHECK_VALID_UNTIL $BACKPORTS_REPO buster-backports main contrib non-free
EOF
else
    cat <<EOF > $BUILDDIR/etc/apt/sources.list
deb$CHECK_VALID_UNTIL $REPO_ROOT/debian bullseye main contrib non-free
EOF
fi

cat <<EOF > $BUILDDIR/etc/locale.gen
en_US.UTF-8 UTF-8
EOF

    update_packages
    install_packages bash-completion bc bzip2 dbus dc dnsutils ed file ftp gawk htop iftop info iotop less \
        libpam-systemd moreutils parted rsync screen sysstat telnet unzip uuid-runtime \
        vim-nox whois jq curl sudo lz4 libreadline7 man-db acl locales iputils-tracepath traceroute \
        bsd-mailx mutt casync strace tftp-hpa conntrack debconf-utils nmap ncat ndiff
    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR debconf-set-selections <<EOF
wireshark-common        wireshark-common/install-setuid boolean true
EOF
    install_packages --no-install-recommends tcpdump wireshark-common
    cp -a $BUILDDIR /root/arm64deb.cache
else
    echo "copying cached"
    cp -aT /root/arm64deb.cache $BUILDDIR
fi

# discard: use TRIM to "zero-out" freed blocks, making the final image smaller
mkdir -p $BUILDDIR/boot/firmware
mount -o discard /dev/mapper/loop0p1 $BUILDDIR/boot/firmware

# remove, not using
rm -f $BUILDDIR/etc/{subuid,subuid-,subgid,subgid-}

mkdir $BUILDDIR/data
chmod 700 $BUILDDIR/data
mkdir $BUILDDIR/raspifirm
chmod 700 $BUILDDIR/raspifirm

# make it smaller
aarch64-linux-gnu-strip /vagrant/target/aarch64-unknown-linux-musl/release/raspi-helper
cp /vagrant/target/aarch64-unknown-linux-musl/release/raspi-helper $BUILDDIR/usr/local/sbin

# copy bash completion
mkdir -p $BUILDDIR/usr/share/bash-completion/completions/
cp /vagrant/raspi-helper/completions/raspi-helper.bash $BUILDDIR/usr/share/bash-completion/completions/raspi-helper

mkdir -p $BUILDDIR/etc/initramfs-tools/hooks/
cat <<'EOF' > $BUILDDIR/etc/initramfs-tools/hooks/raspi
#!/bin/bash

PREREQ=""

prereqs() {
    echo "$PREREQ"
}

case $1 in
prereqs)
    prereqs
    exit 0
    ;;
esac

. /usr/share/initramfs-tools/hook-functions

#copy_exec /usr/local/sbin/raspi-helper
#copy_exec /usr/sbin/mke2fs /usr/local/sbin/mke2fs
#copy_exec /usr/sbin/partprobe /usr/local/sbin/partprobe
#copy_exec /sbin/sfdisk
EOF
chmod +x $BUILDDIR/etc/initramfs-tools/hooks/raspi

mkdir -p $BUILDDIR/etc/initramfs-tools/scripts/local-bottom
cat <<'EOF' > $BUILDDIR/etc/initramfs-tools/scripts/local-bottom/westtel_overlay
#!/bin/sh

set -e

PREREQ=""
prereqs()
{
    echo "$PREREQ"
}

case $1 in
prereqs)
    prereqs
    exit 0
    ;;
esac

${rootmnt}/usr/local/sbin/raspi-helper initramfs-local-bottom

# set time in initramfs before starting systemd
if [ -e ${rootmnt}/etc/fake-hwclock.data ]; then
    SAVED=$(date -u -d "$(cat ${rootmnt}/etc/fake-hwclock.data)" '+%s')
    NOW=$(date -u '+%s')
    if [ $NOW -le $SAVED ]; then
        date -u -s @"$SAVED"
    fi
fi
EOF
chmod a+x $BUILDDIR/etc/initramfs-tools/scripts/local-bottom/westtel_overlay

mkdir -p $BUILDDIR/etc/initramfs-tools/conf.d/
cat <<EOF > $BUILDDIR/etc/initramfs-tools/conf.d/noresume.conf
# disable resume (there is no swap)
RESUME=none
EOF

cat <<EOF > $BUILDDIR/etc/initramfs-tools/modules
squashfs
EOF

if [ $DEBIAN_RELEASE = "bullseye" ]; then
    # allow boot from usb for linux 5.10
    mkdir -p $BUILDDIR/usr/share/initramfs-tools/modules.d/
    cat <<EOF > $BUILDDIR/usr/share/initramfs-tools/modules.d/raspi-firmware.conf
reset-raspberrypi
EOF
fi

# setup coredumps
mkdir -p $BUILDDIR/etc/tmpfiles.d
cat <<EOF > $BUILDDIR/etc/tmpfiles.d/00_override-coredump.conf
# 2755 has sgid bit set, which allows adm group to read the contents
# of ALL coredumps.
d /var/lib/systemd/coredump 2755 root adm 31d
EOF

cat<<EOF > $BUILDDIR/etc/tmpfiles.d/00_systemd-networkd-run.conf
d /run/systemd/network 0750 root systemd-network -
EOF

mkdir -p $BUILDDIR/etc/monit/conf.d
cat <<EOF > $BUILDDIR/etc/monit/conf.d/coredump
check program coredump with path "/bin/bash -c 'ls /var/lib/systemd/coredump; exit $(ls /var/lib/systemd/coredump | wc -l)'" timeout 10 seconds
	if changed status then alert
	if status != 0 then alert
EOF

# setup system configuration
mkdir -p $BUILDDIR/usr/lib/systemd/systemd.conf.d
cat <<EOF > $BUILDDIR/usr/lib/systemd/systemd.conf.d/base.conf
[Manager]
DefaultLimitCORE=infinity
DefaultMemoryAccounting=yes
DefaultCPUAccounting=yes
DefaultIOAccounting=yes
EOF
mkdir -p $BUILDDIR/usr/lib/systemd/journald.conf.d
cat <<EOF > $BUILDDIR/usr/lib/systemd/journald.conf.d/base.conf
[Journal]
SystemMaxUse=1G
EOF
mkdir -p $BUILDDIR/usr/lib/systemd/resolved.conf.d
cat <<EOF > $BUILDDIR/usr/lib/systemd/resolved.conf.d/base.conf
[Resolve]
DNS=8.8.8.8 1.1.1.1 8.8.4.4 1.0.0.1 208.67.222.222 208.67.220.220
DNSSEC=no
EOF

mkdir -p $BUILDDIR/datadisk1
ln -s /var/lib/systemd/coredump $BUILDDIR/datadisk1/coredump

export EXTRAPACKAGES="ssh dosfstools wireguard \
iw systemd-coredump \
monit exim4 chrony/buster-backports iptables-persistent zram-tools dnsmasq"

export MAINPACKAGES="raspi-firmware/buster-backports linux-image-arm64/buster-backports \
firmware-brcm80211/buster-backports crda wireless-regdb/buster-backports lvm2 squashfs-tools \
fake-hwclock lm-sensors"


mkdir -p $BUILDDIR/etc/modprobe.d/
cat <<EOF > $BUILDDIR/etc/modprobe.d/cfg80211.conf
options cfg80211 ieee80211_regdom=US
EOF

mkdir -p $BUILDDIR/etc/default
cat <<EOF > $BUILDDIR/etc/default/crda
REGDOMAIN=US
EOF

cat <<EOF > $BUILDDIR/etc/default/raspi-firmware
ROOTPART=/dev/vg-pi/root-$VERSION
CONSOLES=tty0
EOF

mkdir -p $BUILDDIR/etc/chrony
cat <<EOF > $BUILDDIR/etc/chrony/chrony.conf
pool time.cloudflare.com iburst
pool time.nist.gov iburst
pool pool.ntp.org iburst

driftfile /var/lib/chrony/chrony.drift
ntsdumpdir /var/lib/chrony
logdir /var/log/chrony
makestep 15 -1
EOF

mkdir -p $BUILDDIR/etc/sysctl.d/
cat <<EOF > $BUILDDIR/etc/sysctl.d/99-westtel.conf
net.ipv4.conf.all.rp_filter = 1
EOF

# TODO TODO TODO TODO
mkdir -p $BUILDDIR/etc/exim4
cat <<EOF > $BUILDDIR/etc/exim4/update-exim4.conf.conf
# /etc/exim4/update-exim4.conf.conf
#
# Edit this file and /etc/mailname by hand and execute update-exim4.conf
# yourself or use 'dpkg-reconfigure exim4-config'
#
# Please note that this is _not_ a dpkg-conffile and that automatic changes
# to this file might happen. The code handling this will honor your local
# changes, so this is usually fine, but will break local schemes that mess
# around with multiple versions of the file.
#
# update-exim4.conf uses this file to determine variable values to generate
# exim configuration macros for the configuration file.
#
# Most settings found in here do have corresponding questions in the
# Debconf configuration, but not all of them.
#
# This is a Debian specific file

dc_eximconfig_configtype='smarthost'
dc_other_hostnames=''
dc_local_interfaces='127.0.0.1 ; ::1'
dc_readhost='pi.westtel.com'
dc_relay_domains=''
dc_minimaldns='false'
dc_relay_nets=''
dc_smarthost='smarthost.wg-management.westtel.com'
CFILEMODE='644'
dc_use_split_config='false'
dc_hide_mailname='true'
dc_mailname_in_oh='true'
dc_localdelivery='mail_spool'
EOF
# TODO TODO TODO
cat <<EOF > $BUILDDIR/etc/mailname
pi.westtel.com
EOF

install_packages $MAINPACKAGES $EXTRAPACKAGES

cat <<EOF > $BUILDDIR/etc/iptables/rules.v4
*nat
:POSTROUTING ACCEPT [0:0]
# NAT out of the WiFi device
-A POSTROUTING -o wlan0 -j MASQUERADE
COMMIT
*raw
:PREROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
# drop packets with rpfilter (spoofed)
-A PREROUTING -m rpfilter --invert -j DROP
COMMIT
*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
:TCP - [0:0]
:UDP - [0:0]
-A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -m conntrack --ctstate INVALID -j DROP
# allow pings
-A INPUT -p icmp -m icmp --icmp-type 8 -m conntrack --ctstate NEW -j ACCEPT
-A INPUT -p udp -m conntrack --ctstate NEW -j UDP
-A INPUT -p tcp --tcp-flags FIN,SYN,RST,ACK SYN -m conntrack --ctstate NEW -j TCP
-A INPUT -p udp -j REJECT --reject-with icmp-port-unreachable
-A INPUT -p tcp -j REJECT --reject-with tcp-reset
-A INPUT -j REJECT --reject-with icmp-proto-unreachable

# TCP table, accept tcp here
# ssh and monit, from local and site
-A TCP -p tcp -i eth0 --dport 22 -j ACCEPT
-A TCP -p tcp -i eth0 --dport 2812 -j ACCEPT
-A TCP -p tcp -i wgpi --dport 22 -j ACCEPT
-A TCP -p tcp -i wgpi --dport 2812 -j ACCEPT

# UDP table, accept udp here
# DNS on eth0
-A UDP -p udp -i eth0 --dport 53 -j ACCEPT

# forwarded traffic
-A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
# eth0 <-> wgpi is allowed bidirectionally
-A FORWARD -i eth0 -o wgpi -j ACCEPT
-A FORWARD -i wgpi -o eth0 -j ACCEPT

# only allow out wlan0
-A FORWARD -i eth0 -o wlan0 -j ACCEPT

# block everything else
-A FORWARD -j REJECT --reject-with icmp-host-unreachable
COMMIT
EOF

cat <<EOF > $BUILDDIR/etc/iptables/rules.v6
*raw
:PREROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
-A PREROUTING -m rpfilter --invert -j DROP
# protect the wgmanagement interface from spoofing
-A PREROUTING -i wgmanagement -s fd97:5e16:a609::/48 -j ACCEPT
-A PREROUTING -s fd97:5e16:a609::/48 -j DROP
-A OUTPUT -o wgmanagement -d fd97:5e16:a609::/48 -j ACCEPT
-A OUTPUT -d fd97:5e16:a609::/48 -j DROP
COMMIT
*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
:TCP - [0:0]
:UDP - [0:0]
-A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -m conntrack --ctstate INVALID -j DROP
-A INPUT -s fe80::/10 -p ipv6-icmp -j ACCEPT
-A INPUT -p ipv6-icmp -m icmp6 --icmpv6-type 128 -m conntrack --ctstate NEW -j ACCEPT
-A INPUT -p udp -m conntrack --ctstate NEW -j UDP
-A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -m conntrack --ctstate NEW -j TCP
-A INPUT -p udp -j REJECT --reject-with icmp6-adm-prohibited
-A INPUT -p tcp -j REJECT --reject-with tcp-reset
-A INPUT -j REJECT --reject-with icmp6-adm-prohibited

# TCP table
# allow management from wgmanagement
# SSH
-A TCP -p tcp -i wgmanagement --dport 22 -j ACCEPT
# Monit
-A TCP -p tcp -i wgmanagement --dport 2812 -j ACCEPT
COMMIT
EOF

rm $BUILDDIR/etc/chrony/chrony.keys

if [ ! -d /root/firmware-nonfree ]; then
    pushd /root
    git clone https://github.com/RPi-Distro/firmware-nonfree
    cd firmware-nonfree
    git reset --hard 83938f78ca2d5a0ffe0c223bb96d72ccc7b71ca5
    popd
fi

cp -a $BUILDDIR/usr/lib/firmware/brcm $BUILDDIR/usr/lib/firmware/brcm.bak
cp -rT /root/firmware-nonfree/brcm $BUILDDIR/usr/lib/firmware/brcm

if [ ! -f /root/iwd.tar ]; then
    ./iwd.sh
fi

echo "untar"
cp /root/iwd.tar $BUILDDIR/iwd.tar
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR tar --dereference -xC / -f /iwd.tar
rm $BUILDDIR/iwd.tar

mkdir $BUILDDIR/etc/iwd
cat <<EOF > $BUILDDIR/etc/iwd/main.conf
[General]
EnableNetworkConfiguration=true
[Blacklist]
InitialTimeout=10
MaximumTimeout=60
[Scan]
MaximumPeriodicScanInterval=30
EOF

mkdir -p $BUILDDIR/lib/systemd/system/iwd.service.d
cat <<EOF > $BUILDDIR/lib/systemd/system/iwd.service.d/override.conf
[Service]
ExecStartPre=+/usr/local/sbin/raspi-helper init-iwd
EOF

systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR systemctl enable iwd

mkdir -p $BUILDDIR/lib/systemd/system/systemd-networkd.service.d
cat <<EOF > $BUILDDIR/lib/systemd/system/systemd-networkd.service.d/override.conf
[Service]
ExecStartPre=-+/usr/local/sbin/raspi-helper init-networkd --in-exec-pre
EOF

cat <<EOF > $BUILDDIR/lib/systemd/system/raspi-update-networkd.service
[Unit]
Description=update network configuration
[Service]
Type=oneshot
ExecStart=/usr/local/sbin/raspi-helper init-networkd
EOF

cat <<EOF > $BUILDDIR/lib/systemd/system/raspi-update-networkd.timer
[Unit]
Description=update network configuration
[Timer]
OnBootSec=1min
OnUnitActiveSec=10min

[Install]
WantedBy=timers.target
EOF

cat <<EOF > $BUILDDIR/etc/dnsmasq.conf
clear-on-reload

bind-dynamic
except-interface=lo
interface=eth0

cache-size=1000
EOF

mkdir -p $BUILDDIR/etc/default
cat <<EOF > $BUILDDIR/etc/default/dnsmasq
ENABLED=1
IGNORE_RESOLVCONF=yes
EOF

systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR systemctl enable raspi-update-networkd.timer

do_wpinger aarch64-unknown-linux-musl "--wait-for-chronyc-waitsync"

echo "wtpi-$VERSION" > $BUILDDIR/etc/hostname

# allow root logins locally with no password
# sed -i 's,root:[^:]*:,root::,' $BUILDDIR/etc/shadow

# update fstab (tmpfs /tmp)
cat <<EOF > $BUILDDIR/etc/fstab
# /dev/vg-pi/root / ext4 defaults,errors=remount-ro,noatime 0 1
# LABEL=RASPIFIRM /raspifirm vfat ro,noatime 0 2
# /dev/vg-pi/data /data ext4 defaults,errors=remount-ro,noatime 0 1
tmpfs /tmp tmpfs strictatime,nosuid,nodev,size=512M 0 0
EOF

sed -i 's/cma=64M //' $BUILDDIR/boot/firmware/cmdline.txt
# quiet makes boot a little faster
sed -i 's/ rw / ro quiet /' $BUILDDIR/boot/firmware/cmdline.txt
sed -i 's/cma=$CMA//' $BUILDDIR/etc/kernel/postinst.d/z50-raspi-firmware

systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR systemctl enable systemd-networkd systemd-resolved
# we don't use apt for updates, not useful
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR systemctl disable apt-daily.timer apt-daily-upgrade.timer

# create westtel user home directory
# password created with `mkpasswd --method=sha512crypt`
# westtel user already specified in {passwd,group} so we can't use `useradd`
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR mkhomedir_helper westtel
    #useradd westtel \
    #--groups adm,systemd-journal,sudo --create-home --user-group --shell /bin/bash --non-unique --uid 1000 \
    #--password '$6$d2lVzUTwwz8MBGGx$p6tCOiJlvV/725kJpmOaRAa8NABQTgOqIwmbmnInDxAg34CFMQzZZk0xu.mDcQqYhRDTGPMVrnHKfysUOuZ7j/'

# use deb.debian.org for final image
# if [ $DEBIAN_RELEASE = "buster" ]; then
#     cat <<EOF > $BUILDDIR/etc/apt/sources.list
# deb https://deb.debian.org/debian buster main contrib non-free
# deb https://deb.debian.org/debian-security buster/updates main contrib non-free
# deb https://deb.debian.org/debian buster-backports main contrib non-free
# EOF
# else
#     cat <<EOF > $BUILDDIR/etc/apt/sources.list
# deb https://deb.debian.org/debian bullseye main contrib non-free
# EOF
# fi

# monit setup
do_monit

cat <<EOF > $BUILDDIR/etc/monit/conf.d/fs
check filesystem firmwarefs with path /dev/disk/by-label/RASPIFIRM
    if space > 80% for 60 cycles then alert
    if inode usage > 80% for 60 cycles then alert

check filesystem datafs with path /dev/vg-pi/data
    if space > 80% for 60 cycles then alert
    if inode usage > 80% for 60 cycles then alert
EOF

cat <<EOF > $BUILDDIR/lib/systemd/system/monit.service.d/00westtel.conf
[Service]
ExecStartPre=-/bin/cp /raspifirm/data/00westtel /etc/monit/conf.d/
EOF

do_ssh

do_exim4

# clean up apt info
# systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR apt-get clean
# rm -rf $BUILDDIR/var/lib/apt/lists

echo "removing the package manager..."
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR apt-get purge -y --allow-remove-essential apt

# update resolv.conf
rm $BUILDDIR/etc/resolv.conf
ln -s /run/systemd/resolve/stub-resolv.conf $BUILDDIR/etc/resolv.conf

rm -rf $BUILDDIR/var/log/journal
mkdir $BUILDDIR/var/log/journal

rm -f $BUILDDIR/etc/machine-id $BUILDDIR/var/lib/dbus/machine-id
touch $BUILDDIR/etc/machine-id

pushd $BUILDDIR/boot/firmware
mkdir -p prefix/$VERSION
mv *.dtb cmdline.txt initrd.img-* vmlinuz-* prefix/$VERSION

sponge config.txt <<EOF
os_prefix=prefix/$VERSION/

# get rid of warning
kernel_address=0x200000

# speed up boot
boot_delay=0
disable_splash=1

$(cat config.txt)
EOF

cp config.txt prefix/$VERSION

sha256sum bootcode.bin *.dat *.elf > SHASUMS256.txt
cd prefix/$VERSION
# recompress using "--rsyncable", make updates smaller
<initrd.img-* gunzip | gzip --rsyncable | sponge initrd.img-*
sha256sum * > SHASUMS256.txt
popd

mkdir -p $CASYNCDIR
echo "casync boot prefix"
casync make $CASYNCDIR/$VERSION-prefix.caidx $BUILDDIR/boot/firmware/prefix/$VERSION --without=all

rm -rf /root/firm
mkdir -p /root/firm
pushd $BUILDDIR/boot/firmware
cp SHASUMS256.txt bootcode.bin *.dat *.elf /root/firm
casync make $CASYNCDIR/$VERSION-firmware.caidx /root/firm --without=all
popd

mkdir -p $BUILDDIR/boot/firmware/data/{iwd,network}
cat <<EOF > $BUILDDIR/boot/firmware/data/network/eth0.network
[Match]
Name = eth0
[Network]
Address = 192.168.59.50/32
IPv4ProxyARP = yes
IPForward = yes
# put a route for every device behind it
[Route]
Destination = 192.168.59.105/32
[Route]
Destination = 192.168.59.106/32
EOF

cat <<EOF > $BUILDDIR/boot/firmware/data/network/wgmanagement.netdev
[NetDev]
Name = wgmanagement
Kind = wireguard
Description = WG Management Network

[WireGuard]
# Create a key:
PrivateKey =

[WireGuardPeer]
PublicKey = bfCfkUVvNHcs99TutPrlPdv+lTGhI/otvjjRX45JzRU=
Endpoint = __WG_SERVICE__(wg-management-endpoint.westtel.com)
PersistentKeepalive = 25
AllowedIPs = fd97:5e16:a609::/48
EOF

cat <<EOF > $BUILDDIR/boot/firmware/data/network/wgmanagement.network
[Match]
Name = wgmanagement
[Network]
# PUT IN A VALID ADDRESS HERE
Address = fd97:5e16:a609::1/128

DNS = fd97:5e16:a609:1::1
Domains = ~wg-management.westtel.com ~9.0.6.a.6.1.e.5.7.9.d.f.ip6.arpa
[Route]
Destination = fd97:5e16:a609::/48
EOF

cat <<EOF > $BUILDDIR/boot/firmware/data/network/wgpi.netdev
[NetDev]
Name = wgpi
Kind = wireguard
Description = WG Portable Position

[WireGuard]
# FILL THIS IN
PrivateKey =

[WireGuardPeer]
# FILL THIS IN
PublicKey =
Endpoint = __WG_SERVICE__(portable-wg.test59.wt.infra.westtel.com)
PersistentKeepalive = 25
# allow ALL ips, use other stuff to decide
AllowedIPs = 0.0.0.0/0
EOF

cat <<EOF > $BUILDDIR/boot/firmware/data/network/wgpi.network
[Match]
Name = wgpi
[Network]
IPForward = yes

# this may need to be altered, default send all private ranges here
[Route]
Destination = 192.168.0.0/16
[Route]
Destination = 10.0.0.0/8
[Route]
Destination = 172.16.0.0/12
EOF

cat <<'EOF' > $BUILDDIR/boot/firmware/data/00westtel
# REPLACE $PSAP_NAME

set mail-format {
	from: psap@westtel.com
	subject: $PSAP_NAME  $EVENT $SERVICE at $DATE
	message: $EVENT Service $SERVICE
		Date:        $DATE
		Action:      $ACTION
		Host:        $HOST
		PSAP:        $PSAP_NAME
		Description: $DESCRIPTION
}

# UNCOMMENT TO ACTIVATE ALERTS
# set alert alarm.monit@westtel.com but not on { instance } with reminder on 1440 cycles
EOF

touch $BUILDDIR/boot/firmware/firstboot
umount $BUILDDIR/boot/firmware

# fix LVM
sed -i 's,/etc/lvm/backup,/var/backups/lvm/backup,' $BUILDDIR/etc/lvm/lvm.conf
sed -i 's,/etc/lvm/archive,/var/backups/lvm/archive,' $BUILDDIR/etc/lvm/lvm.conf
rm -rf $BUILDDIR/etc/lvm/{backup,archive}

# add mtab
ln -s ../proc/self/mounts $BUILDDIR/etc/mtab

# clean up var
rm -rf $BUILDDIR/var/log/{apt,alternatives.log,chrony,bootstrap.log,dpkg.log,monit.log} $BUILDDIR/var/lib/{chrony,dbus} $BUILDDIR/var/cache/{apparmor,man}

if ! cmp -s ./etc/passwd $BUILDDIR/etc/passwd || ! cmp -s ./etc/group $BUILDDIR/etc/group || ! cmp -s ./etc/shadow $BUILDDIR/etc/shadow || ! cmp -s ./etc/gshadow $BUILDDIR/etc/gshadow; then
    echo "there is a new user/group id! check /root/etc_debug"
    mkdir -p /root/etc_debug
    cp $BUILDDIR/etc/{passwd,group,shadow,gshadow} /root/etc_debug
    exit 1
fi

mkdir $BUILDDIR/usr/share/factory
mv $BUILDDIR/etc/ $BUILDDIR/usr/share/factory
mv $BUILDDIR/var/ $BUILDDIR/usr/share/factory
mv $BUILDDIR/root/ $BUILDDIR/usr/share/factory
mv $BUILDDIR/home/ $BUILDDIR/usr/share/factory
mkdir $BUILDDIR/{etc,var}
ln -s /data/root $BUILDDIR/root
ln -s /data/home $BUILDDIR/home

pushd $BUILDDIR/usr/share/factory/etc
# remove backups
rm -f passwd- group- shadow- gshadow-
# don't support adding users
rm -f adduser.conf deluser.conf
# we removed apt, don't need the config
rm -rf apt
popd

# empty dev
rm -rf $BUILDDIR/dev
mkdir $BUILDDIR/dev

# empty boot stuff, no longer needed
rm -rf $BUILDDIR/boot
rm -f $BUILDDIR/{vmlinuz,vmlinuz.old,initrd.img,initrd.img.old}

# set clock to now
date -u '+%Y-%m-%d %H:%M:%S' > $BUILDDIR/usr/share/factory/etc/fake-hwclock.data

rm -f /root/squashfsroot

mksquashfs $BUILDDIR /root/squashfsroot
echo "casync squashfs..."
casync make $CASYNCDIR/$VERSION-root.caibx /root/squashfsroot

lvcreate -l 100%FREE -n root-$VERSION vg-pi
dd if=/root/squashfsroot of=/dev/vg-pi/root-$VERSION

# we need to umount it *before* copying the image, otherwise the image gets corrupted
sync
cleanup
trap - EXIT

echo "compressing image..."
<$IMAGE xz -0 -T 0 - > ./portable-pi-$VERSION.img.xz

echo "casync gc"
casync gc $CASYNCDIR/*.caibx $CASYNCDIR/*.caidx

echo "finished version $VERSION"
