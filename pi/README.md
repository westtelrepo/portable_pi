# Notes

If you are mounting it on Linux, Linux will try to mount the LVM.

Add a filter to `/etc/lvm/lvm.conf` (e.g., `"r|/dev/sd.*|"`) to prevent it from
doing this. (Be careful you don't prevent it from mounting the root drive LVM!
This filter will not work in all scenarios.)