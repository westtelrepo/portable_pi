#!/bin/bash

# automated version of https://iwd.wiki.kernel.org/gettingstarted
# creates /root/iwd.tar for build.sh to use

set -e

export SOURCE_DATE_EPOCH=0

export BUILDDIR=/root/iwdbuild
export CACHEDIR=/root/iwdbuild.cache

export REPO_ROOT=http://127.0.0.1:3142
# export REPO_ROOT=https://deb.debian.org

function cleanup {
    echo "cleanup..."
    rm -rf $BUILDDIR
}
trap cleanup EXIT

if [ ! -d $CACHEDIR ]; then
    mkdir -p $BUILDDIR

    echo "debootstrap"
    debootstrap --foreign --arch=arm64 buster $BUILDDIR $REPO_ROOT/debian/
    echo "debootstrap second stage"
    chroot $BUILDDIR /debootstrap/debootstrap --second-stage

    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR -E DEBIAN_FRONTEND=noninteractive apt-get -y install \
        git build-essential libtool libreadline-dev libdbus-glib-1-dev python3-docutils

    # release 0.44
    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR --chdir=/root git clone https://git.kernel.org/pub/scm/libs/ell/ell.git
    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR --chdir=/root/ell git reset --hard 48434d60c646a2f24487a7c5620d8ecfa76263a1

    # release 1.18
    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR --chdir=/root git clone https://git.kernel.org/pub/scm/network/wireless/iwd.git
    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR --chdir=/root/iwd git reset --hard 617d0176411c504e529406dddc0a1967c1496b3e

    cp -a $BUILDDIR $CACHEDIR
else
    echo "copying cache..."
    cp -a $CACHEDIR $BUILDDIR
fi

systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR --chdir=/root/iwd ./bootstrap
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR --chdir=/root/iwd ./configure --prefix=/usr/local --localstatedir=/var --sysconfdir=/etc
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR --chdir=/root/iwd make

systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR --chdir=/root/iwd make install DESTDIR=/root/iwdinstall
systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR --chdir=/root/iwdinstall tar -cf /root/iwd.tar .

cp $BUILDDIR/root/iwd.tar /root/iwd.tar
