These are suggestions, not absolutes.

* rename raspi-helper to something more generic
* turn raspi-helper into a daemon instead:
** SSH disconnections no longer are an issue
** better locking
* port raspi-helper to amd64/UEFI/VMware/etc

# Notes/Possible Directions

Redo the UIDs in the initramfs instead of requiring the UIDs to stay the same on upgrades.

Convert much of `raspi-helper` to use a socket-activated daemon, and have the CLI be an interface to the daemon.
* Makes it trivial to prevent "multiple people updating at same time"
* Disconnection during upgrade doesn't have a chance of screwing things up (the command is just showing the output)
* Can loosen the exclusive lock some commands take (`/raspifirm` stays mounted RW until all that need it RW go away).
* Can expose HTTP port for things like 2FA/WebGUI
* use varlink for CLI-to-daemon communication