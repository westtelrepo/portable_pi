# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "debian/contrib-buster64"

  config.vm.provider "virtualbox" do |vb|
    # configure to something more meaningful for your computer
    vb.memory = "24000"
    vb.cpus = 12
  end

  config.vm.provision "shell", inline: <<-SHELL
    cat <<EOF > /etc/apt/sources.list
deb http://deb.debian.org/debian buster main
deb http://security.debian.org/debian-security buster/updates main
deb https://deb.debian.org/debian buster-backports main
EOF
    apt-get update
    DEBIAN_FRONTEND=noninteractive apt-get install -y dosfstools qemu-user-static/buster-backports binfmt-support kpartx qemu-utils parted fdisk debootstrap curl \
      systemd-container mkosi lvm2 squashfs-tools apt-transport-https ca-certificates curl gnupg moreutils casync zlib1g-dev \
      binutils-aarch64-linux-gnu pkg-config libssl-dev git bc bison flex libssl-dev make libc6-dev libncurses5-dev \
      crossbuild-essential-arm64 apt-cacher-ng/buster-backports

    cat <<EOF > /etc/apt-cacher-ng/remaps.conf
Remap-snapshot: snapshot.debian.org/archive /snapshot ; https://snapshot.debian.org/archive
Remap-secdeb: security.debian.org /debian-security ; https://security.debian.org https://deb.debian.org/debian-security
Remap-debrep: deb.debian.org/debian /debian ; https://deb.debian.org/debian
EOF

    systemctl restart apt-cacher-ng

    # install docker (needed for cross)
    echo \
      "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
$(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    apt-get update
    apt-get install -y docker-ce docker-ce-cli containerd.io

    # let vagrant user use docker without sudo (needed for cross)
    usermod -aG docker vagrant

    # install rust/cargo for vagrant user
    curl https://sh.rustup.rs -sSf | sudo -u vagrant sh -s -- -y

    # cross is for cross compiling
    sudo -u vagrant /home/vagrant/.cargo/bin/cargo install cross cargo-outdated cargo-audit cargo-deny cargo-edit cargo-geiger cargo-tree

    mkdir /home/vagrant/target
    chown vagrant:vagrant /home/vagrant/target

    # install a newer version of squashfs-tools (needed for reproducibility)
    if [ ! -f /usr/local/bin/mksquashfs ]; then
      cd /usr/local/src
      wget https://github.com/plougher/squashfs-tools/archive/4.4-git.1.tar.gz
      tar -xf 4.4-git.1.tar.gz
      cd squashfs-tools-4.4-git.1/squashfs-tools
      make install
    fi
  SHELL

  config.vm.provision "shell", run: "always", inline: <<-SHELL
    mkdir -p /vagrant/target
    mount --bind /home/vagrant/target /vagrant/target
  SHELL
end
