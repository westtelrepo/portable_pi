# this contains some functions that are shared between image builds.

function do_monit {
    rm $BUILDDIR/etc/monit/monitrc
    cat <<'BIGEOF' > $BUILDDIR/usr/local/sbin/gen_monit_conf.sh
#!/bin/bash

if [ -f /etc/monit/monitrc ]; then
    exit 0
fi

cat <<EOF > /etc/monit/monitrc
set daemon 60
    with start delay 60

set mailserver localhost

set httpd port 2812
    allow localhost
    allow 0.0.0.0/0
    allow @adm
    # needed for sudo monit status
    allow monit:$(</dev/urandom tr -dc a-zA-Z0-9 | head -c 16)

# use syslog instead of a log file
set logfile syslog

# the rest of these are default Debian
set idfile /var/lib/monit/id

set statefile /var/lib/monit/state

set eventqueue
    basedir /var/lib/monit/events
    slots 100

include /etc/monit/conf.d/*
include /etc/monit/conf-enabled/*
EOF
    chmod go-rwx /etc/monit/monitrc
BIGEOF
    chmod u+x $BUILDDIR/usr/local/sbin/gen_monit_conf.sh

    rm -rf $BUILDDIR/var/lib/monit

    mkdir $BUILDDIR/lib/systemd/system/monit.service.d

    cat <<EOF > $BUILDDIR/lib/systemd/system/monit.service.d/override.conf
[Service]
Restart=always
RemainAfterExit=no
PIDFile=/run/monit.pid
StateDirectory=monit
ExecStartPre=/usr/local/sbin/gen_monit_conf.sh
EOF

    mkdir -p $BUILDDIR/etc/monit/conf.d
    cat <<'EOF' > $BUILDDIR/etc/monit/conf.d/coredump
check program coredump with path "/bin/bash -c 'ls /var/lib/systemd/coredump; exit $(ls /var/lib/systemd/coredump | wc -l)'" timeout 10 seconds
	if changed status then alert
	if status != 0 then alert
EOF

    cat <<'EOF' > $BUILDDIR/etc/monit/conf.d/00westtel
# REMEMBER TO REPLACE $PSAP_NAME

set mail-format {
	from: psap@westtel.com
	subject: $PSAP_NAME  $EVENT $SERVICE at $DATE
	message: $EVENT Service $SERVICE
		Date:        $DATE
		Action:      $ACTION
		Host:        $HOST
		PSAP:        $PSAP_NAME
		Description: $DESCRIPTION
}

# REMEMBER TO UNCOMMENT
# set alert alarm.monit@westtel.com with reminder on 1440 cycles
# version for pi:
# set alert alarm.monit@westtel.com but not on { instance } with reminder on 1440 cycles
EOF

    cat <<'EOF' > $BUILDDIR/etc/monit/conf.d/host
check system $HOST
	if memory > 60% for 60 cycles then alert
	if memory > 80% for 60 cycles then alert
EOF

    # don't monitor SSH because monit doesn't work well with ssh.socket
    #
    #cat <<EOF > $BUILDDIR/etc/monit/conf.d/ssh
#check process ssh with pidfile /run/sshd.pid
    #if does not exist then alert
#EOF
}

function do_ssh {
    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR systemctl disable ssh.service
    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR systemctl enable ssh.socket

    # generate host keys
    cat <<EOF > $BUILDDIR/usr/local/sbin/gen_host_ssh_keys_if_needed.sh
#!/bin/bash

if [ ! -f /etc/ssh/ssh_host_ecdsa_key ]; then
	ssh-keygen -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key -N ''
fi

if [ ! -f /etc/ssh/ssh_host_ed25519_key ]; then
	ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N ''
fi

if [ ! -f /etc/ssh/ssh_host_rsa_key ]; then
	ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N ''
fi
EOF
    chmod u+x $BUILDDIR/usr/local/sbin/gen_host_ssh_keys_if_needed.sh
    mkdir -p $BUILDDIR/lib/systemd/system/ssh.service.d
    cat <<EOF > $BUILDDIR/lib/systemd/system/ssh.service.d/override.conf
[Service]
ExecStartPre=/usr/local/sbin/gen_host_ssh_keys_if_needed.sh
EOF
    mkdir -p $BUILDDIR/lib/systemd/system/ssh@.service.d
    cat <<EOF > $BUILDDIR/lib/systemd/system/ssh@.service.d/override.conf
[Service]
ExecStartPre=/usr/local/sbin/gen_host_ssh_keys_if_needed.sh
EOF

    cat <<EOF > $BUILDDIR/etc/ssh/ssh_known_hosts
psap.westtel.com,23.24.152.49 ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDEVct/row/lTBT2Uu5XQSmGDoA9jhpys/g4BvpxQFbHeGhg0DYzC+AgWhHJ4OnzhklmLxkbD3XD1eo5i3XKZX2RV5Mfc8al2hgkEEPKzDyskG8b8xreylcszmr7KxBNCzwHkcjmy3TxGPEYLuuYkx0PIY8UwUFHRIWswCD4cylNZfhyExmOpolUgdFYU4fnldEncq4l+6l5X1h6LzFkGco76oDqXPW3a4CAP3YQXIn1G5f6iCAVDX0sUtidm3CJscDzhszo5a7yTvLZ0pOlXlahDXiZOpRnp/+j0aii5nYa8f1OW0dvum/M9yRayTHdgqEO5bahL1R9FU05tgaL0WmxJANFnwEyBU1Wc4sRf1fQ8O6h5HUZ1OHA81eR/YhmWmUWDmAdSRPyRgG16OPUJzqFqrrrdJ5OkQ4AlLb7SY3o6CC/Y4eW80wRMmzgvdRN0GeG+t2qqmB+YU7CdYPFBVzXtOCEphqrDXP+oT09ndFx3mhfwKtsNjsSRjz0UD+3wt1PKvHo8U146x9HUkJNaVWLEfsXNMrU5vX8cQ/8BpHM/FCF6wD+p1MBr+BB8rPpV4bkzGa7U/MhcGn9tmU43aMuQS6REOkdl0ZYevkLDFdYXDOdN1yNWgoYAStYwcjEcIIxy3t75xgY6K+k/WQmlZlJEL10Y+7mZqJ/NTNC3tt1Q=
psap.westtel.com,23.24.152.49 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBGyzgTRaLRuP3tTUOqcRrknep3n1tq+jfozu7C2jLo20dPH0lVF9PKjwI/Z79TAONdYhUje2txtZU/lJYclklFA=
psap.westtel.com,23.24.152.49 ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICF7SCY2wSL4bbOV9b4nKGzMHALKosJdhVvIVjXFBJab
EOF

    rm -f $BUILDDIR/etc/ssh/ssh_host_*
}

function do_exim4 {
    mkdir -p $BUILDDIR/etc/monit/conf.d
    cat <<EOF > $BUILDDIR/etc/monit/conf.d/exim4
check process exim4 with pidfile /var/run/exim4/exim.pid
    start program = "/bin/systemctl start exim4.service"
	stop program = "/bin/systemctl stop exim4.service"
	restart program = "/bin/systemctl restart exim4.service"
    if failed port 25 protocol smtp for 60 cycles then alert
    if does not exist then alert
EOF

    mkdir -p $BUILDDIR/lib/systemd/system/exim4.service.d
    cat <<EOF > $BUILDDIR/lib/systemd/system/exim4.service.d/override.conf
[Service]
RemainAfterExit=no
PIDFile=/run/exim4/exim.pid
Restart=always
EOF
}

function do_systemd_network_perms {
    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR mkdir -p /etc/systemd/network
    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR chmod ug=rw,o= /etc/systemd/network
    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR setfacl -m g:adm:rx,g:systemd-network:rx /etc/systemd/network
}

function do_wpinger {
    if ! [ -f /vagrant/wpinger-$1 ]; then
        return
    fi

    echo "adding wpinger ($1) ($2)..."

    cp /vagrant/wpinger-$1 $BUILDDIR/wpinger
    chmod +x $BUILDDIR/wpinger
    systemd-nspawn -E SOURCE_DATE_EPOCH=0 -aD $BUILDDIR /wpinger self install --systemd-not-booted --systemd-at-least-241 $2
    rm $BUILDDIR/wpinger
}
